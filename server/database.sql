SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `scilab_updates` ;
CREATE SCHEMA IF NOT EXISTS `scilab_updates` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `scilab_updates` ;

-- -----------------------------------------------------
-- Table `scilab_updates`.`components`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `scilab_updates`.`components` ;

CREATE  TABLE IF NOT EXISTS `scilab_updates`.`components` (
  `component_id` INT NOT NULL AUTO_INCREMENT ,
  `component_name` VARCHAR(255) NOT NULL ,
  `component_path` VARCHAR(255) NOT NULL COMMENT 'A comma-separated list of paths belonging to the component, relative to SCI/' ,
  PRIMARY KEY (`component_id`) ,
  INDEX `module_name_index` (`component_name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `scilab_updates`.`versions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `scilab_updates`.`versions` ;

CREATE  TABLE IF NOT EXISTS `scilab_updates`.`versions` (
  `version_id` INT NOT NULL AUTO_INCREMENT ,
  `major` INT NOT NULL ,
  `minor` INT NOT NULL ,
  `maintenance` INT NOT NULL ,
  `revision` INT NULL DEFAULT 0 ,
  `string` VARCHAR(255) NULL ,
  PRIMARY KEY (`version_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `scilab_updates`.`platforms`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `scilab_updates`.`platforms` ;

CREATE  TABLE IF NOT EXISTS `scilab_updates`.`platforms` (
  `platform_id` INT NOT NULL AUTO_INCREMENT ,
  `platform_short_name` VARCHAR(45) NOT NULL ,
  `platform_long_name` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`platform_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `scilab_updates`.`updates`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `scilab_updates`.`updates` ;

CREATE  TABLE IF NOT EXISTS `scilab_updates`.`updates` (
  `update_id` INT NOT NULL AUTO_INCREMENT ,
  `component_id` INT NOT NULL ,
  `platform_id` INT NOT NULL ,
  `old_version_id` INT NOT NULL ,
  `new_version_id` INT NOT NULL ,
  PRIMARY KEY (`update_id`) ,
  INDEX `fk_updates_components` (`component_id` ASC) ,
  INDEX `fk_updates_versions1` (`old_version_id` ASC) ,
  INDEX `fk_updates_versions2` (`new_version_id` ASC) ,
  INDEX `fk_updates_platforms1` (`platform_id` ASC) ,
  CONSTRAINT `fk_updates_components`
    FOREIGN KEY (`component_id` )
    REFERENCES `scilab_updates`.`components` (`component_id` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_updates_versions1`
    FOREIGN KEY (`old_version_id` )
    REFERENCES `scilab_updates`.`versions` (`version_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_updates_versions2`
    FOREIGN KEY (`new_version_id` )
    REFERENCES `scilab_updates`.`versions` (`version_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_updates_platforms1`
    FOREIGN KEY (`platform_id` )
    REFERENCES `scilab_updates`.`platforms` (`platform_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `scilab_updates`.`algorithms`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `scilab_updates`.`algorithms` ;

CREATE  TABLE IF NOT EXISTS `scilab_updates`.`algorithms` (
  `algorithm_real_id` INT NOT NULL ,
  `algorithm_name` VARCHAR(45) NOT NULL COMMENT '@TODO: useless?' ,
  PRIMARY KEY (`algorithm_real_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `scilab_updates`.`patches`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `scilab_updates`.`patches` ;

CREATE  TABLE IF NOT EXISTS `scilab_updates`.`patches` (
  `patch_id` INT NOT NULL AUTO_INCREMENT ,
  `update_id` INT NOT NULL ,
  `file_path` VARCHAR(255) NOT NULL COMMENT 'This path is relative to the module\'s root folder (don\'t forget that a patch is attached to an update which is attached to a module).' ,
  PRIMARY KEY (`patch_id`) ,
  INDEX `fk_patches_updates1` (`update_id` ASC) ,
  CONSTRAINT `fk_patches_updates1`
    FOREIGN KEY (`update_id` )
    REFERENCES `scilab_updates`.`updates` (`update_id` )
    ON DELETE CASCADE
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `scilab_updates`.`concrete_patches`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `scilab_updates`.`concrete_patches` ;

CREATE  TABLE IF NOT EXISTS `scilab_updates`.`concrete_patches` (
  `concrete_patch_id` INT NOT NULL AUTO_INCREMENT ,
  `patch_id` INT NOT NULL ,
  `algorithm_real_id` INT NOT NULL ,
  `stored_patch_filename` VARCHAR(255) NOT NULL COMMENT 'absolute (relative to the root of the filesystem)\n' ,
  `stored_patch_size` INT NOT NULL ,
  PRIMARY KEY (`concrete_patch_id`) ,
  INDEX `fk_concrete_patches_patches1` (`patch_id` ASC) ,
  INDEX `fk_concrete_patches_algorithms1` (`algorithm_real_id` ASC) ,
  INDEX `stored_patch_size` (`stored_patch_size` DESC) ,
  CONSTRAINT `fk_concrete_patches_patches1`
    FOREIGN KEY (`patch_id` )
    REFERENCES `scilab_updates`.`patches` (`patch_id` )
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_concrete_patches_algorithms1`
    FOREIGN KEY (`algorithm_real_id` )
    REFERENCES `scilab_updates`.`algorithms` (`algorithm_real_id` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `scilab_updates`.`platforms`
-- -----------------------------------------------------
START TRANSACTION;
USE `scilab_updates`;
INSERT INTO `scilab_updates`.`platforms` (`platform_id`, `platform_short_name`, `platform_long_name`) VALUES (NULL, 'W86', 'Windows (x86)');
INSERT INTO `scilab_updates`.`platforms` (`platform_id`, `platform_short_name`, `platform_long_name`) VALUES (NULL, 'W64', 'Windows (x64)');
INSERT INTO `scilab_updates`.`platforms` (`platform_id`, `platform_short_name`, `platform_long_name`) VALUES (NULL, 'L86', 'Linux (x86)');
INSERT INTO `scilab_updates`.`platforms` (`platform_id`, `platform_short_name`, `platform_long_name`) VALUES (NULL, 'L64', 'Linux (x64)');
INSERT INTO `scilab_updates`.`platforms` (`platform_id`, `platform_short_name`, `platform_long_name`) VALUES (NULL, 'M', 'Mac OS X');

COMMIT;

from filecmp import dircmp
import os
from sys import stderr, exit, path
from shutil import rmtree, copyfile
from random import randrange
import argparse
import subprocess
from collections import deque
import logging
import exceptions
import traceback
import fnmatch
import re
import copy

from patch_creator_config import *
CONTINUE_FILENAME = 'patch_creator_continue.txt'
CONTINUE_FILENAME_WORK = 'patch_creator_continue_work.txt'

def get_file_size(fn):
    s=os.path.getsize(fn)
    if s < 1024:
        return '{0} B'.format(s)
    elif s < 1024*1024:
        return '{0:2f} KB'.format(s/1024.)
    else:
        return '{0:2f} MB'.format(s/1024./1024.)

"""
def version_check(v):
    vstr=v
    v=map(str.strip,v.split('.'))
    try:
        #are major, minor, maintenance set?
        if len(v) < 3 or len(v) > 5:
            raise Exception

        # are major, minor and maintenance valid integers?
        v[0] = int(v[0])
        v[1] = int(v[1])
        v[2] = int(v[2])
        if v[0] < 0 or v[1] < 0 or v[2] < 0:
            raise Exception

        # revision is set, make sure it's valid integer
        if len(v) > 3 and v[3] != '':
            v[3] = int(v[3]) 
            if v[3] < 0:
                raise Exception
        else:
            v.append(0)

        if len(v) < 5:
            v.append('')

        return v

    except Exception:
        logging.fatal('Invalid version number %s. Proper format is ' \
                'major.minor.maintenance.revision.string' % vstr)
        exit(-1)

    return None
"""

class MiniProcessManager:
    PIPE = subprocess.PIPE
    def __init__(self, maxproc=4):
        self.procs = deque()
        self.maxproc = maxproc

    def spawn(self, cmd, stdout=None, callback=None):
    #def spawn(self, cmd, stdout=PIPE, callback=None):
        while len(self.procs) >= self.maxproc:
            p, cb = self.procs.popleft()
            logging.debug( 'Waiting for %d' % p.pid )
            """if not p.wait():
                logging.critical('Process %d returned non-zero exit code,'
                'bailing out' % p.pid)
                exit(-1)
            """
            p.wait()
            if cb:
                cb()
        p = subprocess.Popen(cmd, stdout=stdout) 
        self.procs.append( (p, callback) )
        logging.debug( 'Spawning %d: %s' % (p.pid, repr(cmd)) )
        return p
    
    def wait(self):
        logging.debug('wait(): Waiting for children...')
        while self.procs:
            p,cb = self.procs.popleft()
            logging.debug( 'wait(): waiting for %d' % p.pid )
            p.wait()
            if cb:
                cb()

class PatchCreator:
    def __init__(self, from_folder, to_folder, 
            output_folder, platform, continue_filename, continue_=False,
            binary_only=False):
        self.from_folder = from_folder
        self.to_folder = to_folder
        self.output_folder = output_folder
        self.binary_only = binary_only
        self.platform = platform
        self.completed_components = set()
        self.continue_filename = continue_filename
        if continue_:
            try:
                with open(continue_filename, 'r') as f:
                    self.completed_components = set(
                            filter(bool,map(str.strip,f.readlines()))
                        )
            except IOError, e:
                print 'Error continuing:', e
                exit(-1)

        assert(os.path.isdir(self.from_folder))
        assert(os.path.isdir(self.to_folder))

    def _patch_path (self, innerpath):
        has_patches = False
        old_dir = os.path.join(self.from_folder, innerpath)
        new_dir = os.path.join(self.to_folder, innerpath)
        
        if self._test_ignore(innerpath) or self._test_ignore(new_dir):
            return False

        if not os.path.isdir(new_dir) and not os.path.isdir(old_dir):
            logging.warning('Neither %s or %s exist' % (old_dir, new_dir))
            return False

        if not os.path.isdir(old_dir):
            self._recursive_report(self.to_folder, innerpath,
                    self._report_added_file)
            return True

        if not os.path.isdir(new_dir):
            self._recursive_report(self.from_folder, innerpath,
                    self._report_deleted_file)
            return True


        cmp = dircmp(old_dir, new_dir)
    
        assert(len(cmp.funny_files) == 0)

        # report all files that are to be deleted from left to right
        if not self.binary_only:
            for f in cmp.left_only:
                f = os.path.join(innerpath, f)
                if self._test_ignore(f):
                    continue
                has_patches = True
                self._recursive_report(self.from_folder, f,
                        self._report_deleted_file)

        # report all files that are to be added from left to right
        if not self.binary_only:
            for f in cmp.right_only:
                f = os.path.join(innerpath, f)
                if self._test_ignore(f):
                    continue
                has_patches = True
                self._recursive_report(self.to_folder, f,
                        self._report_added_file)

        # create patches for all files which are different
        for f in cmp.diff_files:
            outf = os.path.join(innerpath, f)
            srcf = os.path.join(self.from_folder, outf)
            dstf = os.path.join(self.to_folder, outf)
            
            if self._test_ignore(outf):
                continue
            has_patches = True
           
            if self._is_text(srcf) and self._is_text(dstf):
                if not self.binary_only:
                    self._report_text_diff(outf)
            else:
                self._report_binary_diff(outf)

        # apply the patcher recursively
        for dir in cmp.common_dirs:
            dir = os.path.join(innerpath,dir)
            #if self._test_ignore(dir):
            #    continue
            has_patches |= self._patch_path(dir)

        return has_patches


    @staticmethod
    def _recursive_report (absolute_prefix, f, func):
        absolutef = os.path.join(absolute_prefix, f)
        if os.path.isdir(absolutef):
            for ff in os.listdir(absolutef):
                PatchCreator._recursive_report(absolute_prefix,
                        os.path.join(f,ff), func)
        elif os.path.isfile(absolutef):
            func(f)
        else:
            logging.warning('File %s is not normal or directory!' % absolutef)

    #called when moving from one component to another
    def _report_next_component(self, component):
        raise exceptions.NotImplementedError
    
    #called when SCI_FROM/fn doesn't exist in SCI_TO/fn anymore,
    #so the patch should delete it
    def _report_deleted_file (self, fn):
        raise exceptions.NotImplementedError

    #called when SCI_TO/fn is to be added
    def _report_added_file (self, fn):
        raise exceptions.NotImplementedError

    #called when SCI_FROM/outf and SCI_TO/outf differ and are both text files
    def _report_text_diff (self, outf):
        raise exceptions.NotImplementedError
    
    #called when SCI_FROM/outf and SCI_TO/outf differ and are binary files
    def _report_binary_diff (self, outf):
        raise exceptions.NotImplementedError

    def run(self):
        #@TODO: what do we do with the root folder? (/)
        #maybe force a special norecurse for /?

        #load SCI_FROM/.updateignore and SCI_TO/.updateignore
        self._global_ignores = set()
        self._load_ignores(os.path.join(self.from_folder, '.updateignore'),
                '', self._global_ignores)
        self._load_ignores(os.path.join(self.to_folder, '.updateignore'),
                '', self._global_ignores)

        self.from_v = self._scilab_version_find(self.from_folder)
        self.to_v = self._scilab_version_find(self.to_folder)

        self._report_start()

        #everything that's not part of a component is part of scilab
        def components_generator(components):
            for v in components.iteritems():
                yield v
            yield ('scilab', [''])

        
        if self.platform in ['w86', 'w64']:
            components = COMPONENTS_WIN
        else:
            components = COMPONENTS_POSIX
            

        completed_components_f = open(self.continue_filename, 'a+')


        for component, paths in components_generator(components):

            #get the version of the current component, or use the
            #scilab version otherwise
            #if no directories of that component exist, fall back to 0.0.0.0
            #(lowest possible version), unless the component is scilab itself
            from_v = [0,0,0,0,'']
            if component == 'scilab':
                from_v = self.from_v
            for p in paths:
                #no such directory
                if not os.path.isdir(os.path.join(self.from_folder,p)):
                    continue
                v = self._find_version( os.path.join(self.from_folder,p))
                #found a version
                if v:
                    from_v = v
                    break
                else:
                    #keep looking, otherwise fall back to scilab version
                    from_v = self.from_v

            to_v = self.to_v
            for p in paths:
                v = self._find_version(os.path.join(self.to_folder,p))
                if v:
                    to_v = v
                    break

            #components that were diff-ed in a previous run of the software are
            #not reported
            if component not in self.completed_components:
                self._report_next_component(component, from_v, to_v)

            for component_path in paths:

                #if there's an .updateignore, load it

                #first of all, ignore all global ignores (ignores that work on
                #any path)
                self._ignores = copy.copy(self._global_ignores)
                #secondly, ignore everything from SCI_FROM/path/.updateignore
                self._load_ignores(os.path.join(self.from_folder,
                    component_path, '.updateignore'), component_path,
                    self._ignores)
                #thirdly, ignore everything from SCI_TO/path/.updateignore
                self._load_ignores(os.path.join(self.to_folder,
                    component_path, '.updateignore'), component_path,
                    self._ignores)

                #@TODO: shouldn't we add component_path to global_ignores?
                #this means that:
                #1) the order of the components is important
                #2) we can use it to create a final component which means
                #"all the files that aren't in any other component"
                #3) we prevent two components from owning the same file
                self._global_ignores.add(component_path+'/*')

                #if this component was already diff-ed, we only iterated to
                #update our ignore list
                if component in self.completed_components:
                    continue

                has_changes = self._patch_path(component_path)
                if has_changes and from_v == to_v:
                    print ('ERROR: Component %s has changes, but '\
                            'version does not differ' % component)
                    exit(-1)
                elif not has_changes and from_v != to_v:
                    logging.info('Component %s doesn\'t have changes, ' \
                            'but version differs' % component)
            
            if component not in self.completed_components:
                completed_components_f.write(component + '\n')


    #for now, to check if a file is text or binary, simply test if it has any
    #null bytes or >128 bytes
    #FIXME this is probably a bad heuristic
    ##@TODO: write better heuristic
    @staticmethod
    def _is_text(f):
        if f[-4:].lower() in ['.xml','.txt']:
            return True
        if f[-5:].lower() in ['.html']:
            return True

        fh = open(f,'r')
        data = fh.read()
        #BOM?
        #if data[0:3]=='\xFF\xFE\xFF':
        #    return True
        fh.close()
        for c in data:
            if c == '\0' or ord(c)>128:
                return False
        return True

    @staticmethod
    def _load_ignores(from_file, path, into):
        try:
            with file(from_file, 'r') as f:
                into += map(lambda x:os.path.join(path, str.strip(x)),
                    f.readlines())
        except IOError:
            pass

    def _test_ignore(self, fn):
        for ignore in self._ignores:
            if fnmatch.fnmatch(fn, ignore):
                logging.info('%s ignored because it matches %s' % (fn,ignore))
                return True
        return False

    _FIND_METHOD_PATTERN = re.compile(
            r'''
            major="(\d+)"
            .+
            minor="(\d+)"
            .+
            maintenance="(\d+)"
            (?:
            .+
                revision="(\d+)"
                (?:
                .+
                string="(\d+)"
                )?
            )?
            ''',
            re.IGNORECASE|re.MULTILINE|re.VERBOSE)
    @staticmethod
    def _find_version(dir):
        fn = os.path.join(dir, 'version.xml')
        try:
            with open(fn, 'r') as f:
                for line in f.readlines():
                    match = PatchCreator._FIND_METHOD_PATTERN.search(line)
                    if match:
                        #found version info
                        version = [
                                int( match.group(1) ), #major
                                int( match.group(2) ), #minor
                                int( match.group(3) ), #maintenance
                                int( match.group(4) ), #revision
                                match.group(5)         #string
                                ]
                        if not version[3]:
                            version[3] = 0
                        if not version[4]:
                            version[4] = ''
                        return version
        except IOError:
            return False
    
    @staticmethod
    def _pretty_version (v):
        return '.'.join(filter(bool,map(str,v)))

    ##parse version.h to get scilab version
    def _scilab_version_find(self,sci):
        #FIXME @TODO: platform specific test?
        #FIXME it may be somewhere else on Windows
        version_h = os.path.join(sci, 'include',
                'scilab', 'version.h')
        import re
        define = re.compile(r'^\s*#define\s+(\S+)\s+(.+)$')

        v = [None,None,None,None,'']
        with open(version_h, 'r') as vf:
            for line in vf:
                m = define.match(line)
                if not m:
                    continue
                if m.group(1) == 'SCI_VERSION_MAJOR':
                    v[0] = int(m.group(2))
                elif m.group(1) == 'SCI_VERSION_MINOR':
                    v[1] = int(m.group(2))
                elif m.group(1) == 'SCI_VERSION_MAINTENANCE':
                    v[2] = int(m.group(2))
                elif m.group(1) == 'SCI_VERSION_TIMESTAMP':
                    v[3] = int(m.group(2))
                elif m.group(1) == 'SCI_VERSION_STRING':
                    v[4] = m.group(2).replace('"','')
        assert(v[0]) #major
        assert(v[1] != None) #minor
        assert(v[2] != None) #maintenance
        assert(v[3] != None) #revision
        return v
    

class PreviewPatchCreator(PatchCreator):
    def __init__(self, *kargs, **kwargs):
        PatchCreator.__init__(self,*kargs, **kwargs)
        self._current_component = None

    def _report_start(self):
        print 'FROM: Scilab ({ver}) [{path}]'.format( \
                ver=self._pretty_version(self.from_v), \
                path=self.from_folder)
        print '  TO: Scilab ({ver}) [{path}]'.format( \
                ver=self._pretty_version(self.to_v), \
                path=self.to_folder)

    def _report_next_component(self, component, from_v, to_v):
        if self._current_component:
            print '='*80
            print ''

        self._current_component = component
        print '='*30 + ' ' + component + ' ' + '='*30
        print 'FROM: '+self._pretty_version(from_v)
        print '  TO: '+self._pretty_version(to_v)

    def _report_deleted_file(self, fn):
        print 'Deleting '+fn

    def _report_added_file (self, fn):
        print 'Adding '+fn+' ({0})'.format(get_file_size( \
            os.path.join(self.to_folder, fn)) )

    def _report_text_diff (self, outf):
        print '[T] %s' % outf

    def _report_binary_diff (self, outf):
        print '[B] %s' % outf

class WorkingPatchCreator(PatchCreator):

    ## This is a lambda callback, called automatically when a text patch is 
    ##created. This makes sure that if the text patch is larger than the
    ##destination itself, then the destination is used directly
    class LargePatchCorrector:
        ## Updating SCI_FROM/fn -> SCI_TO/fn
        ## Text patch is at output_folder/patch_fn
        def __init__(self, patch_creator_obj, patch_fn, fn):
            #save the state at the time the object was created.
            #this is because by the time this object is called,
            #the WorkingPatchCreator might be already working on another
            #component, so its values cannot be relied on
            self.patch_creator_obj = patch_creator_obj
            self.patch_fn = patch_fn
            self.fn = fn
            self.component = self.patch_creator_obj.current_component
            self.crt_from_v = self.patch_creator_obj.crt_from_v
            self.crt_to_v = self.patch_creator_obj.crt_to_v

        def __call__(self):
            ## is the patch larger than the destination?

            #absolute path OUT_FOLDER/patch_fn
            patch_full_path = os.path.join(self.patch_creator_obj.output_folder,
                    self.patch_fn)
            #absolute path SCI_TO/fn
            absfn = os.path.join(self.patch_creator_obj.to_folder,
                    self.fn)

            patch_size = os.path.getsize(patch_full_path)
            dest_size = os.path.getsize(absfn)
            if patch_size >= dest_size:
                logging.info('Destination file %s smaller than its text '
                    'patch, giving up on the text patch' % self.fn)
                self.patch_creator_obj.add_patch(255, self.crt_from_v,
                        self.crt_to_v, self.fn, self.patch_fn,
                        self.component)
                copyfile(absfn, patch_full_path)
            else:
                logging.debug('Text patch vs. whole file (%d/%d)' % (patch_size,
                    dest_size))
                self.patch_creator_obj.add_patch(2, self.crt_from_v,
                        self.crt_to_v, self.fn, self.patch_fn,
                        self.component)

    def __init__(self, *kargs, **kwargs):
        PatchCreator.__init__(self, *kargs, **kwargs)
        self.current_component = None
        self._track = open(os.path.join(self.output_folder,'track.txt'), 'w+')

    #@TODO: version aware
    def add_patch(self, alg, from_v, to_v, fn, patch_fn, component=None):
        if not component:
            component = self.current_component

        self._track.write(':'.join( \
            [component, \
             '|'.join(map(str,from_v)), \
             '|'.join(map(str,to_v)), \
             str(alg), \
             fn, \
             patch_fn]) + '\n')
    
    @staticmethod
    def _create_patch_filename(f):
        #get the filename and the extension of f
        fn_parts = os.path.splitext(os.path.basename(f))
        #create a patch filename out of it
        patch_fn = fn_parts[0] + '-' + str(randrange(0,10000)) + fn_parts[1]
        return patch_fn

    def _report_start(self):
        self._track.write(self.platform + '\n')

    def _report_next_component(self, component, from_v, to_v):
        self.current_component = component
        #the versions of the current component (self.from_v and self.to_v
        #are the versions of scilab)
        self.crt_from_v = from_v
        self.crt_to_v = to_v

    def _report_deleted_file(self, f):
        self.add_patch(254, self.crt_from_v, self.crt_to_v, f, '')

    def _report_added_file(self, f):
        patch_fn = self._create_patch_filename(f)
        self.add_patch(255, self.crt_from_v, self.crt_to_v, f, patch_fn)
        copyfile(os.path.join(self.to_folder, f),
                os.path.join(self.output_folder, patch_fn))
        
    def _report_text_diff(self, f):
        patch_fn = self._create_patch_filename(f)
        fromf = os.path.join(self.from_folder, f)
        tof   = os.path.join(self.to_folder, f)
        fd = open(os.path.join(self.output_folder, patch_fn), 'w+')
        self.process_manager.spawn([DIFF_APP, '-e', fromf, tof], fd,
                callback=WorkingPatchCreator.LargePatchCorrector(
                    self, patch_fn, f) )
        
    def _report_binary_diff(self, f):
        patch_fn = self._create_patch_filename(f)
        from_f = os.path.join(self.from_folder, f)
        to_f = os.path.join(self.to_folder, f)
        self.algorithm.create(self, f, patch_fn, self._completed_binary_diff)

    def _completed_binary_diff(self, id, from_v, to_v, fn, patch_fn, component):
        self.add_patch(id, from_v, to_v, fn, patch_fn, component)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Patch an application')
    parser.add_argument('-f', '--from', dest='from_', action='store', type=str,
            default=DEFAULT_FROM_FOLDER, help='The older version of Scilab')
    parser.add_argument('-t', '--to', dest='to', action='store', type=str,
            default=DEFAULT_TO_FOLDER, help='The newer version of Scilab')
    parser.add_argument('-o', '--output', dest='output', action='store',
            type=str, default=DEFAULT_OUTPUT_FOLDER, help='The directory ' \
            'which will be filled the patch files and the updates.txt. Be '\
            'VERY CAREFUL when picking this folder, because the contents of ' \
            'this folder are first emptied, so you will use any files ' \
            'stored there.')
    parser.add_argument('--silent', dest='silent', action='store_true',
            default=False, help='Suppress output.')
    parser.add_argument('-a','--algorithm', dest='algorithm', action='store',
            type=str, default='bsdiff', help='The name of the '
            'algorithm to be used for binary patching')
    parser.add_argument('-b', '--binary', dest='binary_only',
            action='store_true', default=False, help='With this option, ONLY '
            'binary files are diff-ed (i.e. patches for text files are not '
            'created). Files to be added/deleted are not added to the patch '
            'either. The sole purpose of this switch is to compare various '
            'binary patchers, do NOT use.')
    parser.add_argument('-p', '--processes', dest='procs', action='store',
            type=int, default=4, help='The maximum number of processes that '
            'can be spawned simultaneously')
    parser.add_argument('-l', '--log', dest='loglevel', action='store',
            type=str, default='WARN', help='The loglevel')
    parser.add_argument('-w', '--work', dest='do_work', action='store_true',
            default=False, help='By default, the patch is only printed, ' \
                    'not written. Don\'t use this option directly, ' \
                    'only use after running without -w first.')
    parser.add_argument('-P', '--platform', dest='platform', action='store',
            choices=serverconfig.VALID_PLATFORMS, required=True,
            help='The platform for which this patch is created')
    parser.add_argument('-c', '--continue', dest='continue_',
            action='store_true', default=False, help='Whenever '
            'patch_creator fails and you fix the problem, '
            'you can resume without starting from scratch by '
            'using this switch.')
    args = parser.parse_args()

    if not args.algorithm.replace('_','').isalpha():
        stderr.write('Invalid algorithm name.\n')
        exit(-1)
    if args.procs < 1 or args.procs > 128:
        stderr.write('Invalid number of processes.')
        exit(-1)

    logging.basicConfig(level=args.loglevel)
    work = args.do_work
    logging.info('info!')
    logging.warning('warning!')
    logging.critical('critical!')
    
    if work:
        continue_filename = CONTINUE_FILENAME_WORK
    else:
        continue_filename = CONTINUE_FILENAME

    if not args.continue_:
        try:
            os.unlink(continue_filename)
        except OSError:
            pass

    patch_creator_ctor_params = [args.from_, args.to, args.output,
            args.platform, continue_filename, args.continue_, args.binary_only]

    if work:
        if not args.continue_:
            # create the output folder
            try:
                rmtree(args.output)
            except:
                pass
            
            os.mkdir(args.output)

        process_manager = MiniProcessManager(args.procs)
        p = WorkingPatchCreator(*patch_creator_ctor_params)
        p.process_manager = process_manager
        p.silent = args.silent
        
        try:
            p.algorithm = \
                __import__('updaters.'+args.algorithm.lower(), globals(), \
                    locals(), ['*'])
        except ImportError, e:
            stderr.write('Invalid algorithm name: %s\n' % e)
        
    else:
        p = PreviewPatchCreator(*patch_creator_ctor_params)

        if os.path.isdir(p.output_folder) and not args.continue_:
            logging.warning('Patch output folder (%s) already exists and ' \
                    'will be overwritten by -w(--work).' % p.output_folder)
    
    try:
        p.run()
        logging.debug('Main process done.')
    except Exception, e:
        logging.error('Uncaught exception: %s' % str(e))
        traceback.print_exc()
        exit(-1)

    if work:
        process_manager.wait()
        print 'Done.'
        logging.debug('Done waiting for child processes.')

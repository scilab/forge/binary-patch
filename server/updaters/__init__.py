import os, os.path
import logging
from random import randrange

PATCHAPI_APP = \
	'C:\\Program Files\\Microsoft SDKs\\Windows\\v7.1\\Bin\\mpatch.exe'

XDELTA_APP = \
	'D:/gsoc/binary-patch/server/updaters/xdelta3.0z.x86-64.exe'

BSPATCH_APP = \
	'D:/gsoc/binary-patch/server/updaters/bsdiff.exe'

COURGETTE_APP = \
	'D:/gsoc/binary-patch/server/updaters/courgette.exe'

TMP_DIR = \
        '/tmp/'
#    'D:/gsoc/binary-patch/server/updaters/tmp/'

PYTHON_PATH = 'python'

PATCH_CREATOR_PATH = 'D:/gsoc/binary-patch/server/patch_creator.py'

logging.debug('Initializing uploader')

if not os.path.isdir(TMP_DIR):
    os.mkdir(TMP_DIR)

def rand_str(l=12):
    ret = ''
    for i in xrange(0,l):
        ret += chr(randrange(ord('a'), ord('z')+1))
    return ret


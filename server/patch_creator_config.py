import os
from sys import path

#DIFF_APP = 'diff.exe'
DIFF_APP = 'diff'
## The path to the older version of Scilab (overwritable with -f)
DEFAULT_FROM_FOLDER  = 'C:/Program Files/scilab-5.2.2'
## The path to the newer version of Scilab (overwritable with -t)
DEFAULT_TO_FOLDER = 'C:/Program Files/scilab-5.3.1'
## The output folder of the patch files (overwritable with -o)
DEFAULT_OUTPUT_FOLDER = 'E:/tmp/scilab-patch'
## Used to find the serverconfig.py file to get the database credentials
## @TODO: (currently unused)
DOCROOT_FOLDER = os.path.join(os.path.dirname(__file__), 'docroot')
path.append(DOCROOT_FOLDER)
import serverconfig

#@TODO: generate a list of components, fix the scilab component problem
ALGORITHMS = \
{
    254 : 'Delete',
    255 : 'Copy whole file',
    2 : 'Text diff (ed)',
    3 : 'xdelta',
    4 : 'bsdiff',
    5 : 'Patchapi',
    6 : 'Courgette'
}
MODULES_WIN = set([
    'mexlib', 'development_tools', 'simulated_annealing', 'data_structures',
    'symbolic', 'special_functions', 'tclsci', 'javasci', 'boolean', 'api_scilab',
    'io', 'maple2scilab', 'm2sci', 'demo_tools', 'pvm', 'string', 'completion',
    'functions', 'graphic_export', 'console', 'randlib', 'parameters', 'cacsd',
    'history_manager', 'graph', 'localization', 'spreadsheet', 'overloading',
    'xcos', 'scicos_blocks', 'ui_data', 'fileio', 'jvm', 'signal_processing',
    'action_binding', 'interpolation', 'core', 'shell', 'sparse', 'helptools',
    'intersci', 'elementary_functions', 'update', 'polynomials', 'atoms',
    'differential_equations', 'scicos', 'statistics', 'matio', 'windows_tools',
    'arnoldi', 'graphics', 'integer', 'parallel', 'types', 'sound',
    'modules_manager', 'compatibility_functions', 'hdf5', 'linear_algebra',
    'double', 'dynamic_link', 'fftw', 'genetic_algorithms', 'commons', 'texmacs',
    'optimization', 'call_scilab', 'scinotes', 'time', 'umfpack',
    'history_browser', 'renderer', 'gui', 'output_stream'
])
MODULES_POSIX = set([
    'action_binding', 'api_scilab', 'arnoldi', 'atoms', 'boolean', 'cacsd',
    'call_scilab', 'commons', 'compatibility_functions', 'completion',
    'console', 'core', 'data_structures', 'demo_tools', 'development_tools',
    'differential_equations', 'double', 'doublylinkedlist', 'dynamiclibrary',
    'dynamic_link', 'elementary_functions', 'fftw', 'fileio', 'functions',
    'genetic_algorithms', 'graph', 'graphic_export', 'graphics', 'gui',
    'hashtable', 'hdf5', 'helptools', 'history_browser', 'history_manager',
    'integer', 'interpolation', 'intersci', 'io', 'javasci', 'jvm', 'libst',
    'linear_algebra', 'localization', 'm2sci', 'malloc', 'maple2scilab',
    'matio', 'mexlib', 'modules_manager', 'optimization', 'output_stream',
    'overloading', 'parallel', 'parameters', 'polynomials', 'pvm', 'randlib',
    'renderer', 'scicos', 'scicos_blocks', 'scinotes', 'shell',
    'signal_processing', 'simulated_annealing', 'sound', 'sparse',
    'special_functions', 'spreadsheet', 'statistics', 'string', 'symbolic',
    'tclsci', 'texmacs', 'time', 'types', 'ui_data', 'umfpack',
    'windows_tools', 'xcos'])


##maps a component to its relative path(s)
COMPONENTS_WIN = {}
for module in MODULES_WIN:
    COMPONENTS_WIN[module] = ['modules/' + module]

COMPONENTS_POSIX = {}
for module in MODULES_POSIX:
    COMPONENTS_POSIX[module] = ['share/scilab/modules/' + module]


from filecmp import dircmp
import os
from sys import stderr, exit
from shutil import rmtree, copyfile
from random import randrange
from updaters import system, quote
import argparse
import subprocess
from collections import deque

class MiniProcessManager:
    PIPE = subprocess.PIPE
    def __init__(self, maxproc=4):
        self.procs = deque()
        self.maxproc = maxproc
        pass

    def spawn(self, cmd, stdout=None):
        while len(self.procs) >= self.maxproc:
            p = self.procs.popleft()
            print 'Waiting for %d' % p.pid
            p.wait()
        p = subprocess.Popen(cmd, stdout=stdout) 
        self.procs.append(p)
        print 'Spawning %d: %s' % (p.pid, repr(cmd))
        return p
    
    def __del__(self):
        while self.procs:
            p = self.procs.popleft()
            print 'Finally waiting for %d' % p.pid
            p.wait()

# Configuration starts here
######################################################################
DIFF_APP = 'diff.exe'
SRC_FOLDER  = 'C:/Program Files/scilab-5.2.2'
DEST_FOLDER = 'C:/Program Files/scilab-5.3.1'
UPDATES_FOLDER = 'D:/www/updates'
######################################################################
# Configuration ends here

def add_patch(updater, filename, patchname):
    track.write(':'.join([str(updater),filename,patchname]) + '\n')

def create_patch_filename(f):
    #get the filename and the extension of f
    fn_parts = os.path.splitext(os.path.basename(f))
    #create a patch filename out of it
    patch_fn = fn_parts[0] + '-' + str(randrange(0,10000)) + fn_parts[1]
    return patch_fn

def add_all(f):
    absolutef = os.path.join(DEST_FOLDER, f)
    if os.path.isdir(absolutef):
        for ff in os.listdir(absolutef):
            add_all(os.path.join(f,ff))
    elif os.path.isfile(absolutef):
        #updater 255 = copy the contents
        patch_fn = create_patch_filename(f)
        add_patch(255, f, patch_fn)
        copyfile(absolutef, os.path.join(UPDATES_FOLDER,patch_fn))


#for now, to check if a file is text or binary, simply test if it has any
#null bytes or >128 bytes
#this is probably a bad heuristic, but should be enough for this PoC
##@TODO: write better heuristic
def is_text(f):
    fh = open(f,'r')
    data = fh.read()
    fh.close()
    for c in data:
        if c == '\0' or ord(c)>128:
            return False
    return True

def comp_folders(innerpath):
    #apply 255 if the file is to be created
    #apply 254 if file is to be deleted
    #apply 3 if binary
    #apply 2 if text
    
    cmp = dircmp(os.path.join(SRC_FOLDER,innerpath),
            os.path.join(DEST_FOLDER,innerpath))

    assert(len(cmp.funny_files) == 0)

    # report all files that are to be deleted from left to right
    if not binary_only:
        for f in cmp.left_only:
            add_patch(254, os.path.join(innerpath,f), '')

    # report all files that are to be added from left to right
    if not binary_only:
        for f in cmp.right_only:
            add_all(os.path.join(innerpath,f))

    # create patches for all files which are different
    for f in cmp.diff_files:
        patch_fn = create_patch_filename(f)
        srcf = os.path.join(SRC_FOLDER, innerpath, f)
        dstf = os.path.join(DEST_FOLDER, innerpath, f)
        if is_text(srcf) and is_text(dstf):
            if binary_only:
                continue
            ##@TODO: will newlines in the diff file be a problem?
            ##@TODO: will the file be closed automatically when the subprocess
            ##stops?
            fd=open(os.path.join(UPDATES_FOLDER, patch_fn), 'w+')
            #proc=subprocess.Popen([DIFF_APP, srcf, dstf], stdout=fd)
            process_manager.spawn([DIFF_APP, srcf, dstf], fd)
            add_patch(2, os.path.join(innerpath, f), patch_fn)
        else:
            algorithm.create(process_manager, srcf, dstf,
                   os.path.join(UPDATES_FOLDER, patch_fn))
            add_patch(algorithm.id, os.path.join(innerpath, f), patch_fn)

        
    # apply the patcher recursively
    for dir in cmp.common_dirs:
        comp_folders(os.path.join(innerpath,dir))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Patch an application')
    parser.add_argument('-s', '--src', dest='src', action='store', type=str,
            default=SRC_FOLDER, help='The older version of Scilab')
    parser.add_argument('-d', '--dest', dest='dest', action='store', type=str,
            default=DEST_FOLDER, help='The newer version of Scilab')
    parser.add_argument('-o', '--output', dest='output', action='store',
            type=str, default=UPDATES_FOLDER, help='The directory which will '
            'be filled the patch files and the updates.txt. Be VERY CAREFUL '
            'when picking this folder, because the contents of this folder '
            'are first emptied, so you will use any files stored there.')
    parser.add_argument('--silent', dest='silent', action='store_true',
            default=False, help='Suppress output.')
    parser.add_argument('-a','--algorithm', dest='algorithm', action='store',
            type=str, default='xdelta', help='The name of the '
            'algorithm to be used for binary patching')
    parser.add_argument('-b', '--binary', dest='binary_only',
            action='store_true', default=False, help='With this option, ONLY '
            'binary files are diff-ed (i.e. patches for text files are not '
            'created). Files to be added/deleted are not added to the patch '
            'either. The sole purpose of this switch is to compare various '
            'binary patchers, do NOT use.')
    args = parser.parse_args()

    SRC_FOLDER = args.src
    DEST_FOLDER = args.dest
    UPDATES_FOLDER = args.output
    silent = args.silent
    binary_only = args.binary_only
    if not args.algorithm.isalpha():
        stderr.write('Invalid algorithm name.\n')
        exit(-1)
    try:
        algorithm = __import__('updaters.'+args.algorithm.lower(), globals(),
                locals(), ['*'])
    except ImportError, e:
        stderr.write('Invalid algorithm name: %s\n' % e)

    try:
        rmtree(UPDATES_FOLDER)
    except:
        pass

    os.mkdir(UPDATES_FOLDER)

    track = open(os.path.join(UPDATES_FOLDER,'updates.txt'), 'w+')

    process_manager = MiniProcessManager()

    comp_folders('.')

import os, sys
sys.path.append(os.path.join('..','docroot'))

from common import *
from cStringIO import *
import unittest

class MyStringIOTest(unittest.TestCase):

    def test_basic_read_until(self):
        s = StringIO('test1|test2|test3')
        a = MyStringIO(s)
        self.assertEqual( a.read_until('|'), 'test1')
        self.assertEqual( a.read_until('|'), 'test2')
        self.assertEqual( a.read_until('|', True), 'test3')

    def test_block_size(self):
        for i in xrange(1,200):
            self.__test_second(i)

    def __test_second(self, i):
        s = StringIO('This is a test\n'
                'to see how things work\n')

        a = MyStringIO(s)
        self.assertEqual( a.read_until('\n', block_size=i),
                'This is a test' )
        self.assertEqual( a.read_until('\n', block_size=i),
                'to see how things work')

    def test_getattr(self):
        a = MyStringIO(StringIO('hello'))
        self.assertEqual(a.read(), 'hello')
        self.assertEqual(a.getvalue(), 'hello')
        a.seek(0)
        self.assertEqual(a.readlines(), ['hello'])

    def test_longdelim(self):
        a = MyStringIO(StringIO('hello world  we are testing  ' \
            'double delimiters '))
        self.assertEqual( a.read_until('  ', block_size=1),
                'hello world')
        self.assertEqual( a.read_until('  ', block_size=2),
                'we are testing')
        self.assertEqual( a.read_until('  ', block_size=3,
                allow_eof_delimiter=True), 'double delimiters ')

    def test_peek(self):
        a = MyStringIO(StringIO('Testing peek'))
        self.assertEqual(a.peek(1), 'T')
        self.assertEqual(a.peek(1), 'T')
        self.assertEqual(a.peek(1), 'T')
        self.assertEqual(a.peek(), 'Testing peek')
        a.read(4)
        self.assertEqual(a.peek(), 'ing peek')
        self.assertEqual(a.peek(5), 'ing p')
        a.seek(0)
        self.assertEqual(a.peek(), 'Testing peek')
        self.assertEqual(a.peek(), 'Testing peek')


class VersionClassTest(unittest.TestCase):
    def test_serializer(self):
        v = Version()
        v.major = 5
        v.minor = 3
        v.maintenance = 2
        v.revision = 37
        v.string = 'testing testing'
        self.assertEqual(v.serialize(), '5|3|2|37|testing testing|')
        self.assertEqual(str(v), '5.3.2.0037')
        self.assertEqual(repr(v), '5.3.2.0037')

    def test_deserializer(self):
        stream = MyStringIO( \
                StringIO('5|3|2|37|testing testing|IRRELEVANT DATA'))

        v = Version.deserialize_some(stream)
        self.assertEqual(v.major, 5)
        self.assertEqual(v.minor, 3)
        self.assertEqual(v.maintenance, 2)
        self.assertEqual(v.revision, 37)
        self.assertEqual(v.string, 'testing testing')
        self.assertEqual(stream.read(), 'IRRELEVANT DATA')

#@TODO: bother to also test Update?

if __name__ == '__main__':
    unittest.main()

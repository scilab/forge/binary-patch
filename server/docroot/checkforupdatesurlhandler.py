import sys, web
from cStringIO import StringIO
from common import *
import traceback

class CheckForUpdatesUrlHandler:
    #TODO: verify user-agent
    def POST(self):
        #import rpdb2
        #rpdb2.start_embedded_debugger('updater')

        input = web.webapi.input('p', 'v', 'pl')
        if input.v != '1':
            raise web.BadRequest()

        #convert from unicode
        input.p = str(input.p)
        input.pl = str(input.pl)
    
        with MyStringIO(StringIO(input.p)) as stream:
            try:
                if input.pl not in VALID_PLATFORMS:
                    raise Exception, 'Invalid platform'
                parsed_input = self._parse_input(stream)
                parsed_input['platform'] = input.pl
                return self._report_available_updates(**parsed_input)
            except Exception, e:
                traceback.print_exc()
                raise web.BadRequest()

    def _parse_input(self, stream):
        components  = {}
        
        while stream.peek(1) != ':':
            component_name = stream.read_until(':', block_size=8)
            if component_name in components:
                raise IOError('Describing same component twice')
           
            components[component_name] = Version.deserialize_some(stream)

        #get rid of the :
        stream.read(1)

        components['scilab'] = Version.deserialize_some(stream)
        
        return {'components' : components}
        
    def _report_available_updates(self, components, platform):
        #print >>sys.stderr, 'components=',components
        #print >>sys.stderr, 'platform=',platform

        return reduce(lambda x,y: x+Update.serialize(y),
                db.get_updatable_components(components, platform), '')


from common import *
from cStringIO import StringIO
from serverconfig import *
import traceback
from random import randrange
from sys import stderr

class DownloadUpdatesUrlHandler:
    def GET(self):
        return self.POST()

    def POST(self):
        input = web.webapi.input('p', 'v', 'pl')
        if input.v != '1':
            raise web.BadRequest()

        input.p = str(input.p)
        input.pl = str(input.pl)
        

        with MyStringIO(StringIO(input.p)) as stream:
            try:
                if input.pl not in VALID_PLATFORMS:
                    raise Exception, 'Invalid platform'
                data = DownloadUpdatesUrlHandler._parse_input(stream)
                data['platform'] = input.pl
                return DownloadUpdatesUrlHandler._pack_updates(**data)
            except Exception, e:
                traceback.print_exc()
                raise web.BadRequest()

    @staticmethod
    def _parse_input(stream):
        updates = []
        while stream.peek(1) != ':':
            updates.append( Update.deserialize_some(stream) )

        #get rid of :
        stream.read(1)
        
        updaters = set( map(int, stream.read_until('||').split('|')) )
        if not updaters.issubset(VALID_ALGORITHMS):
            raise Exception, 'Invalid patching algorithm(s) in: ' + \
                repr(updaters)
        if not REQUIRED_ALGORITHMS.issubset(updaters):
            raise Exception, 'Insufficient patching algorithms in: '+ \
                repr(updaters)
        return {'updates' : updates, 'updaters' : updaters}

    @staticmethod
    def _to_random_patch_filename(fn):
        fn, ext = os.path.splitext( os.path.basename(fn) )
        return fn+'-'+str(randrange(0,100000))+ext

    @staticmethod
    def _pack_updates(updates, updaters, platform):
        #returns a list of tuples (algid, path_rel_to_SCI,
        #local_path_to_patch)
        patches = db.make_patch(updates, updaters, platform)

        web.header('Content-Type', 'application/x-bzip-compressed-tar')

        ## contents of updates.txt
        updates_txt = ''

        ## tar all files into tarstream
        #@TODO: now tar-ing into file, fails to work properly into memory.
        #print >>stderr, patches
        import tarfile
        tarstream = StringIO()
        tarh = tarfile.open(fileobj=tarstream, mode='w:bz2')
            
        ## add the patches
        for algid, path_rel_to_sci, local_path_to_patch in patches:
            if not local_path_to_patch:
                continue
            fn = DownloadUpdatesUrlHandler._to_random_patch_filename( 
                    path_rel_to_sci)
            tarh.add(local_path_to_patch, fn)
            print 'Tar-ing '+local_path_to_patch+'( '+path_rel_to_sci+')'
            updates_txt += (
                    ':'.join((str(algid), str(path_rel_to_sci), str(fn)) )
                    + '\n' )

        ## add the updates.txt file
        tarinfo = tarfile.TarInfo('updates.txt')
        #get rid of unicode
        updates_txt = str(updates_txt)
        tarinfo.size = len(updates_txt)
        tarh.addfile(tarinfo, fileobj=StringIO(updates_txt))
        #print 'Tar-ing updates.txt ({} B)'.format(tarinfo.size)

        tarh.close()

        with open('/tmp/last_tar.tar.bz2', 'w+') as fh:
            fh.write(tarstream.getvalue())

        
        ## send tarstream to the user
        ## @TODO: gzip compression (Content-Encoding?)
        return tarstream.getvalue()

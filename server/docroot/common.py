import cStringIO, os
from serverconfig import *
import exceptions
import web.db


"""wrapper around StringIO which adds read_until and peek"""
class MyStringIO:

    class EarlyEof(IOError):
        def __init__(self, *kargs, **kwargs):
            Exception.__init__(self, *kargs, **kwargs)

    def __init__(self, fo):
        self.fo = fo

    """
    delim is the delimiter
    allow_eof_delimiter is a boolean var indicating whether it is acceptable to
        treat EOF as the delimiter
    skip_delimiter indicates whether the delimiter should be removed from the
        string or not
    block_size indicates a guess of how far the delimiter is.
    """
    def read_until(self, delim, allow_eof_delimiter=False, skip_delimiter=True,
            block_size=1024):
        s = ''
        while True:
            crts = self.fo.read(block_size)
            if s=='' and crts=='':
                raise MyStringIO.EarlyEof()

            ##single character delimiter, search in crts
            if len(delim) == 1:
                pos = crts.find(delim)
                if pos != -1:
                    pos += len(s)
                s += crts
            ##multi character delimiter, search in s
            else:
                s += crts
                pos = s.find(delim)
            
           
           ##we've reached EOF without finding our delimiter
            if pos == -1 and crts == '':
                ##is that okay?

                #no, throw an exception
                if not allow_eof_delimiter:
                    raise IOError, 'Read until EOF without finding the ' \
                            'delimiter'
                #yes, return all the data
                return s
            
            ##we haven't found the delimiter but it's not EOF yet, keep reading
            elif pos == -1:
                continue

            # 0 ... pos ... len(s)
            # * ...  |  ...     *
            # we return [0,pos) and seek to pos+1

            if skip_delimiter:
                # BS = len(s) = read block size
                # POS+len(delim) = BS - (BS - POS - len(delim))
                #       = BS -BS + POS + len(delim)
                #       = POS + len(delim)
                # therefore, we have to seek -(BS-POS-len(delim))
                # = POS+len(delim)-BS
                # in order to get to POS+len(delim)
                self.fo.seek(pos+len(delim)-len(s), os.SEEK_CUR)
            else:
                self.fo.seek(pos-len(s), os.SEEK_CUR)
            return s[0:pos]

    def peek(self, n=None):
        if not n:
            c=self.fo.read()
        else:
            c=self.fo.read(n)
        if c=='':
            raise IOError, "Peeking at EOF"
        self.fo.seek(-len(c), os.SEEK_CUR)
        return c

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.fo.close()

    def __getattr__(self, attr):
        return getattr(self.fo, attr)

class Version:
    def __init__(self):
        self.major = self.minor = self.maintenance = None
        self.revision = self.string = None

    def serialize(self):
        return '%d|%d|%d|%d|%s|' % (self.major, self.minor, self.maintenance,
                self.revision, self.string)
        
    @staticmethod
    def deserialize_some(stream):
        ret = Version()
        propr = {'delim' : '|', 'allow_eof_delimiter' : False,
                'skip_delimiter' : True, 'block_size' : 8}
        ret.major = int( stream.read_until(**propr) )
        try:
            ret.minor = int( stream.read_until(**propr) )
            ret.maintenance = int( stream.read_until(**propr) )
            ret.revision = int( stream.read_until(**propr) )
            ret.string = stream.read_until(**propr)
        except MyStringIO.EarlyEof:
            raise IOError, 'Deserialization failed.'

        return ret

    def __str__(self):
        return '{self.major}.{self.minor}.' \
               '{self.maintenance}.{self.revision:0>4}'.format(self=self)

    def __repr__(self):
        return self.__str__()

    def __eq__(self, b):
        return [self.major, self.minor, self.maintenance, self.revision] == \
            [b.major, b.minor, b.maintenance, b.revision]

    def __lt__(self, b):
        return [self.major, self.minor, self.maintenance, self.revision] < \
            [b.major, b.minor, b.maintenance, b.revision]

    def __ne__(self, b):
        return [self.major, self.minor, self.maintenance, self.revision] != \
            [b.major, b.minor, b.maintenance, b.revision]

    def __le__(self, b):
        return [self.major, self.minor, self.maintenance, self.revision] <= \
            [b.major, b.minor, b.maintenance, b.revision]
    
    
class Update:
    def serialize(self):
        return self.component_name + '|' + \
            self.old_version.serialize() + \
            self.new_version.serialize()

    @staticmethod
    def deserialize_some(stream):
        propr = {'delim' : '|', 'allow_eof_delimiter' : False,
                'skip_delimiter' : True, 'block_size' : 8}
        ret = Update()
        ret.component_name = stream.read_until(**propr)
        ret.old_version = Version.deserialize_some(stream)
        ret.new_version = Version.deserialize_some(stream)
        return ret

"""
check for updates:
    in: { 'component' : version, ... }
    out: [ Update, ... ]
download updates:
    in: [ Update, ... ]
        [updater, ...]
    out: { 'component' : [ (algorithm, sci_file, patch_file), ... }
"""

#@TODO: I don't like the fact that web.db provides sqlquote() independent
#of the database you're connected to. Maybe switch to MySQLDb directly?
## @TODO: the output of these function can be cached
class DatabaseQueries:
    def __init__(self):
        self.db = web.db.database(
                dbn=dbconfig.DB_TYPE, db=dbconfig.DB_NAME,
                user=dbconfig.DB_USER, pw=dbconfig.DB_PASS,
                host=dbconfig.DB_HOST)

    def _query(self, q):
        q = map(str.strip, q.split('\n'))
        q = filter(bool, q)
        q = ' '.join(q)
        #print ':: '+q
        return self.db.query(q)

    def _escape(self, s):
        return web.db.sqlquote(s)

            
    def make_patch(self, updates, updaters, platform):
        self._patch_accumulator = []
        self._available_updaters_list = ','.join(map(str,map(int,updaters)))
        self._platform = self._get_platformid(platform)
        for update in updates:
            self._find_component_updates(update.component_name,
                    self._platform, update.old_version, update.new_version,
                    self._make_patch_callback)

        return self._patch_accumulator



    ##@TODO: cacheable
    ##@TODO: prepared statements?
    def get_updatable_components(self, components, platform):
        updateable = []
        for component, version in components.iteritems():
            newv = self._find_component_updates(component,
                    self._get_platformid(platform), version)

            if newv != version:
                u = Update()
                u.component_name = component
                u.old_version = version
                u.new_version = newv
                updateable.append(u)
        return updateable

    def _get_platformid(self, platform_short):
        q=self._query('''
        SELECT platform_id FROM platforms
        WHERE platform_short_name = %s
        LIMIT 1
        ''' % self._escape(platform_short))
        if not q:
            raise exceptions.RuntimeError, 'No such platform (%s)' % \
                platform_short
        return q[0].platform_id

    #@TODO: problems with recursion too deep, ever?
    #look for updates for component `component', starting at version `version',
    #and look no further than the version `stop_version'.
    #calls callback(component_id, update_id)
    def _find_component_updates(self, component, platform_id,
            version, stop_version=None, callback=None):

        if stop_version and stop_version < version:
            return version

        #@TODO: we're using int(), which is problematic after 2**32
        #@TODO
        
        ##!!!IMPORTANT!!! FIXME TODO
        ##WE ARE NOT RELYING ON THE STRING OF THE VERSION ANYMORE
        ##PUT THE COMMENTED CODE BACK WHENEVER VERSION STRINGS ARE MORE
        ##CONSISTENT (I REMOVED THIS BECAUSE THE STABLE RELEASES ACTUALLY COME
        ##WITH A unstable-git VERSION STRING, WHICH DOESN'T MAKE MUCH SENSE)
        #################
        q=self._query('''
        SELECT updates.update_id as update_id, newv.major as M, newv.minor as m,
        newv.maintenance as mm, newv.revision as r, newv.string as s,
        updates.component_id as comp
        FROM updates
        JOIN versions AS newv
        ON updates.new_version_id = newv.version_id
        WHERE
        updates.platform_id = {platform_id} AND
        updates.component_id = (SELECT components.component_id
            FROM components
            WHERE components.component_name = {component}
            LIMIT 1)
        AND
        updates.old_version_id = (SELECT versions.version_id
            FROM versions
            WHERE versions.major = {major} AND
            versions.minor = {minor} AND
            versions.maintenance = {maintenance} AND
            versions.revision = {revision} /*AND
            versions.string = {string}*/ LIMIT 1)
        LIMIT 1'''.format(
            platform_id=int(platform_id),
            component=self._escape(component),
            major=int(version.major),
            minor=int(version.minor),
            maintenance=int(version.maintenance),
            revision=int(version.revision),
            string=self._escape(version.string)))

        if q:
            newv = Version()
            q = q[0]
            newv.major = q.M; newv.minor = q.m; newv.maintenance = q.mm;
            newv.revision = q.r; newv.string = q.s;
            if version == newv:
                raise exceptions.RuntimeError, "Mapping from version {0} " \
                    "to itself for component={1}, platform_id={2}".format(
                            version, component, platform_id)
            if callback:
                callback(q.comp, q.update_id)

            if stop_version and stop_version == newv:
                return newv

            return self._find_component_updates(component, platform_id,
                    newv, stop_version, callback)
        else:
            return version


    #@TODO: fix this query
    def _make_patch_callback(self, component_id, update_id):
        q=self._query('''
        SELECT p.patch_id as pid, p.algorithm_real_id as alg,
        patches.file_path AS fn, p.stored_patch_filename AS patch,
        p.stored_patch_size AS size
        FROM concrete_patches AS p
        JOIN patches ON p.patch_id = patches.patch_id
        WHERE patches.update_id = {update_id} AND
        p.algorithm_real_id IN ({available_updaters_list})
        '''.format(
            update_id=int(update_id),
            available_updaters_list=self._available_updaters_list
        ))

        #patches = { patch_id : (algorithm_id, scifile, patch_fn, patch_size) }
        patches = {}
        
        ##out of all the algorithms for one patch, pick the one with the
        ##smallest size.
        for patch in q:
            #either the patch is new or is already there, but we've found
            #an algorithm for which the patch is smaller
            if (patch.pid not in patches) or \
                    patches[patch.pid][3] > patch.size:
                patches[patch.pid] = (patch.alg, patch.fn, \
                        patch.patch,patch.size)

        for patch in patches.itervalues():
            self._patch_accumulator.append( (patch[0], patch[1], patch[2]) )

db = DatabaseQueries()

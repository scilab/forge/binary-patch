<?php
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 'Off');
ini_set('log_errors', 'On');
define('PATH_TO_UPDATES', '../updates');

const USER_AGENT_PATTERN = '@^Scilab-Updater/1\.0$@i';
//const USER_AGENT_PATTERN = 'm@@i';

class Version
{
    public $major, $minor, $maintenance, $revision, $string;

    //@TODO: private?
    const PATTERN = '%d|%d|%d|%d|%s|';
    const PATTERN_REGEXP = '@^(\d+)\|(\d+)\|(\d+)\|(\d+)\|([^|]+)\|(.+)$@';

    public function serialize()
    {
        return sprintf(self::PATTERN,
            $this->major, $this->minor,
            $this->maintenance, $this->revision, $this->string);
    }

    static public function deserialize_some(&$from)
    {
        $ret = new Version;
        if (!preg_match(self::PATTERN_REGEXP, $from, $m))
            throw new Exception('Couldn\'t deserialize version: '.$from);

        $ret->major = (int)$m[1];
        $ret->minor = (int)$m[2];
        $ret->maintenance = (int)$m[3];
        $ret->revision = (int)$m[4];
        $ret->string = $m[5];
        $from = $m[6];
        return $ret;
    }
}

class Update
{
    public $module_name, $old_version, $new_version;

    public function serialize()
    {
        return sprintf('%s|%s%s',
            $module_name,
            $old_version->serialize(),
            $new_version->serialize());
    }
    static public function deserialize_some(&$from)
    {
        $pipe_pos = strpos($from, '|');
        if ($pipe_pos === false)
            throw new Exception('Couldn\'t deserialize update.');
        $update = new Update;
        $update->module_name = substr($from, 0, $pipe_pos);
        $from = substr($from, $pipe_pos+1);
        $update->old_version = Version::deserialize_some($from);
        $update->new_version = Version::deserialize_some($from);
    }
}

//@TODO: use offsets instead for less copying, way too much overhead.
function check_for_updates (&$data)
{
    //modules version
    $modules = array();
    while ($data != ':')
    {
        $colon_pos = strpos($data, ':');
        if ($colon_pos === false)
            throw new Exception('Deserialization failed.1');
        $module_name = substr($data, 0, $colon_pos);
        $data = substr($data, $colon_pos+1);

        if (in_array($module_name, $modules))
            throw new Exception('Deserialization failed.2');

        $modules[$module_name] = Version::deserialize_some($data);
    }
    $data = substr($data, 1);

    //scilab version
    $modules['scilab'] = Version::deserialize_some($data);

    $updaters = array();
    while ($data != '|')
    {
        $pipe_pos = strpos($data, '|');
        if ($pipe_pos === false)
            throw new Exception('Deserialization failed.3');

        array_push($updaters, (int)substr($data, 0, $pipe_pos));
        $data = substr($data, $pipe_pos+1);
    }
    $data = substr($data, 1);

    trigger_error('modules: '.print_r($modules,true), E_USER_NOTICE);
    trigger_error('updaters: '.print_r($updaters,true), E_USER_NOTICE);

    return '';
}



//From here on, code that interfaces with the client (a.k.a. the view)

//most annoying php_ini setting (besides register_globals) xD
if (get_magic_quotes_gpc())
	trigger_error('Turn magic_quotes_gpc off', E_ERROR);

if (!isset($_SERVER['HTTP_USER_AGENT'])) $user_agent = '';
else $user_agent = $_SERVER['HTTP_USER_AGENT'];

if ($_SERVER['REQUEST_METHOD'] != 'POST' || 
    !preg_match(USER_AGENT_PATTERN, $user_agent))
{
    trigger_error("REQUEST_METHOD={$_SERVER['REQUEST_METHOD']} ".
        "USER_AGENT=$user_agent IP={$_SERVER['REMOTE_ADDR']}", E_USER_WARNING);
    die();
}

try
{
    if (!isset($_POST['v']) || $_POST['v'] != '1')
        throw new Exception('Invalid serialization version.');
    if (!isset($_POST['p']) || empty($_POST['p']))
        throw new Exception('No data from user.');
    
    $data = $_POST['p'];

    trigger_error("Got an update query: v={$_POST['v']} p={$_POST['p']}",
        E_USER_NOTICE);

    print check_for_updates($data);

}
catch (Exception $e)
{
    $trace = serialize($e->getTrace());
    trigger_error("Exception " .
        "(UA: $user_agent) [BT: $trace] [data: $data]: $e", E_USER_WARNING);
}

//if (mt_rand(0,1) == 0)
//{
	////report that the user is up-to-date
	//echo '-';
//}
//else
//{
    //echo '!';
	//perform some work here to decide exactly what to send to the user
	//for now, we assume we have to send the whole updates/ folder
	//note: the updates/updates.txt file contains info about which files
	//are to be patched and using which updaters
	
	//archive the files with tar
    /*
	$ph = popen("tar cf - ".escapeshellarg(PATH_TO_UPDATES), "rb");
	if (!$ph) trigger_error("Couldn't execute tar", E_ERROR);

	$data = stream_get_contents($ph);
	pclose($ph);

	//Send the tar (compressed)
	header('Content-Type: application/x-tar');
	header('Content-Length: '.strlen($data));
	ob_start('ob_gzhandler')
		or trigger_error("Couldn't start buffering",E_ERROR);
	echo $data;
	ob_end_flush()
		or trigger_error("Couldn't flush buffer", E_ERROR);
     */
//}
?>

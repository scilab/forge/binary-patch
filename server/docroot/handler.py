import sys
import os

sys.path += [os.path.dirname(__file__)]

import web

from checkforupdatesurlhandler import CheckForUpdatesUrlHandler
from downloadupdatesurlhandler import DownloadUpdatesUrlHandler
#from updateradmin import UpdaterAdminUrlHandler

urls = (
    '/check_for_updates', 'CheckForUpdatesUrlHandler',
    '/download_updates', 'DownloadUpdatesUrlHandler'#,
    #'/admin/(.*)', 'AdminUrlHandler'
    )

def notfound():
    #\TODO: replace to ''
    return web.notfound('wtf not found')

def internalerror():
    #\TODO: replace to ''
    return web.internalerror('wtf internal error')

'''
Taken from http://code.google.com/p/modwsgi/wiki/DebuggingTechniques
'''
'''
class Debugger:

    def __init__(self, object):
        self.__object = object

    def __call__(self, *args, **kwargs):
        import pdb, sys
        debugger = pdb.Pdb()
        debugger.use_rawinput = 0
        debugger.reset()
        sys.settrace(debugger.trace_dispatch)

        try:
            return self.__object(*args, **kwargs)
        finally:
            debugger.quitting = 1
            sys.settrace(None)
web.application = Debugger(web.application)
'''
''' '''


application = web.application(urls, locals()).wsgifunc()
application.notfound = notfound
application.internalerror = internalerror

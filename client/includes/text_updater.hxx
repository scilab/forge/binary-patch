#if !defined(INCLUDED_TEXT_UPDATER_HXX)
#define INCLUDED_TEXT_UPDATER_HXX

#include <string>
#include "updater.hxx"

//! An updater that can be used for text files (text diff)
class text_updater : public updater
{
public:
	static unsigned int UPDATER_ID;
	text_updater() { }

	bool apply_update (std::string const &filename,
		std::string const &patchfile);

	unsigned int get_updater_id() const { return UPDATER_ID; }

	std::string get_updater_name () const { return "Text files updater"; }
};

#endif
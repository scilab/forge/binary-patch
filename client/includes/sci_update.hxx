#if !defined(INCLUDED_UPDATE_HXX)
#define INCLUDED_UPDATE_HXX

//check for updates, download and apply.
extern "C" __declspec(dllexport) int sci_update(
	char *fname,unsigned long fname_len);

#endif
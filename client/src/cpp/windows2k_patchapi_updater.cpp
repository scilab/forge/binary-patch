#if defined(_WIN32)

#include "windows2k_patchapi_updater.hxx"

#include <cassert>
#include <Windows.h>
#include <PatchApi.h>

unsigned int windows2k_patchapi_updater::UPDATER_ID = 1;

bool windows2k_patchapi_updater::apply_update(std::string const &filename, 
	std::string const &patchfile)
{
	return ApplyPatchToFileExA(patchfile.c_str(),
		filename.c_str(), filename.c_str(), APPLY_OPTION_FAIL_IF_EXACT,
		NULL, NULL) == TRUE;
	return true;
}

//register this updater with the manager
namespace
{
	//create an updater creator function
	updater *create_windows2k_patchapi_updater()
	{ return new windows2k_patchapi_updater; }

	//pass that function to the updater manager, so others can create such
	//updaters
	static bool windows2k_patchapi_updater_dummy_value = 
		updater_manager::get_instance().register_updater(
		windows2k_patchapi_updater::UPDATER_ID,
		create_windows2k_patchapi_updater);
}

#endif
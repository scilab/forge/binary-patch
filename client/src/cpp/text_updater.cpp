#include "text_updater.hxx"

#include <cstdlib>

unsigned int text_updater::UPDATER_ID = 2;

bool text_updater::apply_update (std::string const &filename,
		std::string const &patchfile)
{
	//for now, we'll just call diff
	//(available on windows via unxutils)
	
#	ifdef _WIN32
	//change to your path here
	static const std::string patch_binary = "D:/bin/wbin/patch.exe";
#	else
	static const std::string patch_binary = "patch";
#	endif
	
	// shell escaping on filename and patchname wouldn't hurt
	// but it doesn't really matter since this won't be used in the real code
	return std::system( (patch_binary+" "+filename+" "+patchfile).c_str() ) == 0;
}

// register
namespace
{
	updater *create_text_updater () { return new text_updater; }
	static bool text_updater_dummy_val = 
		updater_manager::get_instance().register_updater(
		text_updater::UPDATER_ID, &create_text_updater);
}

#include "send_whole_file_updater.hxx"

#ifdef _WIN32
#include <Windows.h>
#else
#include <copyfile.h>
#endif

unsigned int send_whole_file_updater::UPDATER_ID = 255;

bool send_whole_file_updater::apply_update (std::string const &filename,
		std::string const &patchfile)
{
#	ifdef _WIN32
	return CopyFileA(patchfile.c_str(), filename.c_str(), FALSE)==TRUE;
#	else
	return copyfile(patchfile.c_str(), filename.c_str(), NULL, COPYFILE_ALL) == 0;
#	endif
}

//register the updater to the manager
namespace
{
	updater *create_send_whole_file_updater() { return new send_whole_file_updater; }
	static bool send_whole_file_updater_dummy = 
		updater_manager::get_instance().register_updater(
		send_whole_file_updater::UPDATER_ID,
		&create_send_whole_file_updater);
}
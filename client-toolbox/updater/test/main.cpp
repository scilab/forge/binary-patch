
#include	"UnappliedUpdatesStore.hxx"
#include	<vector>
#include	<string>
#include	<cassert>

bool vcomp(std::vector<std::string> const &v)
{ assert(v.empty()); return true; }

bool vcomp(std::vector<std::string> const &v, std::string const &s)
{ assert(v.size() == 1); assert(v[0] == s); return true; }

bool vcomp(std::vector<std::string> const &v,
        std::string const &s1,
        std::string const &s2)
{
    assert(v.size() == 2); assert(v[0] == s1); assert(v[0] == s2); 
    return true;
}

int main ()
{
    typedef UnappliedUpdatesStore U;

    {
        U u1;
        assert(u1.open());
        assert(!u1.areUnappliedUpdates());
        assert(vcomp(u1.getUpdatePaths()));
    }
    {
        U u2;
        assert(u2.open(true));
        u2.appendUpdate("1");
        //assert(vcomp(u2.getUpdatePaths(true),"1"));
        u2.appendUpdate("2");
        u2.markUpdatesToBeApplied();
        //assert(vcomp(u2.getUpdatePaths(),"1", "2"));
    }
    {
        U u3;
        assert(u3.open());
        assert(vcomp(u3.getUpdatePaths(),"1", "2"));
    }
    {
        U u4;
        assert(u4.open(true));
        assert(vcomp(u4.getUpdatePaths(),"1", "2"));
        u4.popUpdate();
    }
    {
        U u5;
        assert(u5.open());
        assert(vcomp(u5.getUpdatePaths(), "2"));
    }
    {
        U u6;
        assert(u6.open(true));
        assert(vcomp(u6.getUpdatePaths(), "2"));
        u6.popUpdate();
    }
    {
        U u7;
        assert(u7.open());
        assert(!u7.areUnappliedUpdates());
        assert(vcomp(u7.getUpdatePaths()));
    }
    return 0;
}

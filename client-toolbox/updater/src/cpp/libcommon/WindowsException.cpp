
#ifdef _WIN32

#include	"WindowsException.hxx"
#include	<Windows.h>

////////////////////////////////////////////////////////////
//Implementation of class WindowsException//
////////////////////////////////////////////////////////////


std::string WindowsException::formatWindowsMessage (
    std::string const &msg)

{
    LPSTR winErr;
    FormatMessageA(
            FORMAT_MESSAGE_ALLOCATE_BUFFER |
            FORMAT_MESSAGE_FROM_SYSTEM |
            FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL, GetLastError(), 0, (LPSTR)&winErr, 0, NULL);
    
    // remove the newline and everything after it.
    std::string winErrStr(winErr);
    std::string::size_type newline_pos;
    if ( (newline_pos = winErrStr.find_first_of("\r\n")) != std::string::npos)
    
    {
        winErrStr.assign(winErrStr.begin(), winErrStr.begin()+newline_pos);
    }

    std::string ret = msg + " [" + winErrStr + "]";
    LocalFree(winErr);

    return ret;
}

#endif

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////


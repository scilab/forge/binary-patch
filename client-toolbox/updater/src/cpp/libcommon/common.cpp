
#include	"common.hxx"
#include	"UnappliedUpdatesStore.hxx"


//@TODO: maybe only use libarchive on linux?
//#include	<sciprint.h> // @TODO: remove, debug purposes only


//@TODO: remove, this is just for MessageBoxA
//#pragma comment(lib,"user32.lib")

std::string string_replace_first(std::string &string,
        std::string const &from, std::string const &to)
{
    size_t i = string.find(from);
    return string = std::string(string.begin(),string.begin()+i) +
        to +
        std::string(string.begin()+i+from.size(),string.end());
}

std::string string_replace_first(std::string const &string,
        std::string const &from, std::string const &to)
{
    std::string temp(string);
    return string_replace_first(temp, from, to);
}

std::string cpp_printf(std::string const &format, std::string const &str)
{
    return string_replace_first(format, "%s", str);
}
std::string cpp_printf(std::string const &format, double d)
{
    std::ostringstream os; os<<d;
    return string_replace_first(format, "%f", os.str());
}
std::string cpp_printf(std::string const &format, int i)
{
    std::ostringstream os; os<<i;
    return string_replace_first(format, "%d", os.str());
}

std::string cpp_printf(std::string const &format, unsigned int i)
{
    std::ostringstream os; os<<i;
    return string_replace_first(format, "%d", os.str());
}

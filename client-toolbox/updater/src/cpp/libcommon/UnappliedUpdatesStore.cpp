
#ifdef _WIN32
//Warning: this header needs to be included early, because somewhere in its
//headers it uses the identifier Top, which is a #define in some scilab header
//Note: we need to link with shell32.lib to have SHGetFolderPath
#include	<Shlobj.h> // used to get the appdata folder using SHGetFolderPath

#else
#include	<sys/types.h>
#include	<sys/stat.h>
#include	<fcntl.h>
#include	<unistd.h>
#endif

#include	"UnappliedUpdatesStore.hxx"
#include	<cassert>
#include	<sstream>
#include	<cstdlib>
#include	"common.hxx"
#include	"UpdaterErrorCodes.hxx"
#include	<algorithm>
#include	<version.h>

#ifdef _WIN32
template <class It>
static std::string windows_fix_newlines (It start, It end)
{

    std::string fixed_str;

    for (It it = start; it != end; ++it)
    
    {
        if (*it != '\r') fixed_str += *it;
    }
    return fixed_str;
}

#endif

UnappliedUpdatesStore::UnappliedUpdatesStore ()
    : f_(NULL), popped_(0), initialized_updates_(false), path_()
{
    std::string path;
#ifdef _WIN32
    char appdata[MAX_PATH];
    //@TODO: could there be a problem with unicode directory names?
    if (SHGetFolderPathA(NULL, CSIDL_APPDATA, NULL,
                SHGFP_TYPE_CURRENT, appdata) != S_OK)
        throw UpdaterException(UpdaterErrorCodes::CouldNotGetAppdataDirectory,
                true);
    path = appdata + "\\Scilab";
    if (!CreateDirectoryA(path.c_str(), NULL))
    {
        if (GetLastError() != ERROR_ALREADY_EXISTS)
            throw UpdaterException(UpdaterErrorCodes::CouldNotCreateDirectory,
                    true)(path.c_str());
    }
    path = path + "\\" + SCI_VERSION_STRING;
    if (!CreateDirectoryA(path.c_str(), NULL))
    {
        if (GetLastError() != ERROR_ALREADY_EXISTS)
            throw UpdaterException(UpdaterErrorCodes::CouldNotCreateDirectory,
                    true)(path.c_str());
    }
    path_ = path + "\\unapplied_updates.txt";
#else
    //@TODO:
    //a) if scilab is not installed, or you're not running as root, use
    //  ~/scilab/versionid/
    //otherwise, use /var/lib/scilab/versionid/unapplied_updates
    char *env_home = getenv("HOME");
    if (!env_home)
        throw UpdaterException(UpdaterErrorCodes::CouldNotLocateHomeDirectory);
   
    path = env_home + std::string("/.Scilab");
    if (mkdir(path.c_str(), 0755) && errno != EEXIST)
        throw UpdaterException(UpdaterErrorCodes::CouldNotCreateDirectory,
                true)(path);
    
    path = path + "/" + SCI_VERSION_STRING;
    if (mkdir(path.c_str(), 0755) && errno != EEXIST)
        throw UpdaterException(UpdaterErrorCodes::CouldNotCreateDirectory,
                true)(path);

    path_ = path + "/unapplied_updates";
#endif
}
bool UnappliedUpdatesStore::open_(unsigned int op)
{
#ifdef _WIN32
    DWORD open_type;
    switch (op)
    {
        case 0:
            open_type = OPEN_EXISTING;
            break;
        case 1:
            open_type = OPEN_ALWAYS;
            break;
        case 2:
            open_type = TRUNCATE_EXISTING;
            break;
    }
    f_ = CreateFileA(path_.c_str(),
            GENERIC_READ | (op >= 1 ? GENERIC_WRITE : 0),
            op >= 1 ? 0 : FILE_SHARE_READ,
            NULL,
            open_type,
            FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS,
            NULL);
    if (f_ == INVALID_HANDLE_VALUE) { f_ = NULL; return false; }
#else
    int flags;
    switch (op)
    {
    case 0:
        flags = O_RDONLY|O_CREAT|O_DIRECT;
        break;
    case 1:
        flags = O_RDWR|O_CREAT|O_DIRECT;
        break;
    case 2:
        flags = O_RDWR|O_CREAT|O_DIRECT|O_TRUNC;
        break;
    }
    f_ = ::open(path_.c_str(), flags, 0644);
    if (f_ == -1) return false;

    //lock the whole file
    struct flock lock;
    lock.l_type = (op == 0 ? F_RDLCK : F_WRLCK);
    lock.l_whence = SEEK_SET;
    lock.l_start = 0;
    lock.l_len = 0;
    if (fcntl(f_, F_SETLK, &lock) == -1) { close(f_); return false; }

#endif

    return true;
}

bool UnappliedUpdatesStore::areUnappliedUpdates(bool onlyIfNeedToApply)
{
    std::stringstream ss;
    char buf[4096]; 
#ifdef _WIN32
    //move to beginning of file
    SetFilePointer(f_, 0, 0, FILE_BEGIN);

    //read the whole file into stringstream ss
    DWORD bytes_read;
    while ( ReadFile(f_, buf, sizeof(buf), &bytes_read, NULL) && bytes_read)
    {
        //ReadFile() will read with a \r\n otherwise.
        ss << windows_fix_newlines(buf, buf+bytes_read) << std::endl;
    }
#else
    //move to the beginning of file
    if (lseek(f_, 0, SEEK_SET) == (off_t)-1) return false;
    
    //read the whole file into stringstream ss
    //@TODO: don't we need a newline fix on OS X too?
    ssize_t bytes_read;
    while ( (bytes_read = read(f_, buf, 4096)) > 0)
    {
        ss << std::string(buf, buf+bytes_read) << std::endl;
    }

#endif
    std::string yesno, crtline;
    
    initialized_updates_ = false;
    updates_.clear();
    initialized_updates_ = true;

    if (!std::getline(ss, yesno))
        return false;
    if (onlyIfNeedToApply && yesno != "Y")
        return false;

    initialized_updates_ = false;
    if (!onlyIfNeedToApply)
    {
        //there are unapplied updates (there's a Y on the first line)
        while (std::getline(ss, crtline))
        {
            updates_.push_back(crtline);
        }
        initialized_updates_ = true;
        return !updates_.empty();
    }
    else return std::getline(ss,crtline);
}

std::vector<std::string> const &UnappliedUpdatesStore::getUpdatePaths()
{
    if (!initialized_updates_) areUnappliedUpdates();
    return updates_;
}

void UnappliedUpdatesStore::appendUpdate(std::string const &update)
{
    std::string string_to_write_to_file = update + "\r\n";
#ifdef _WIN32
    //seek to the end of file (append)
    SetFilePointer(f_, 0, 0, FILE_END);


    DWORD file_size;
    if ( (file_size = GetFileSize(f_, NULL)) == INVALID_FILE_SIZE)
        throw UpdaterException(UpdaterErrorCodes::CouldNotOpenFile, true)
            (path_);

    //if the file is empty, start by adding a N
    if (!file_size) string_to_write_to_file = "N\r\n" + 
        string_to_write_to_file;

#else
    //seek to the end of file (append)
    off_t file_size = lseek(f_, 0, SEEK_END);
    
    //if the file is empty, start by adding a N
    //@TODO: what to use instead of \n for OS X?
    if (!file_size) string_to_write_to_file = "N\n" + 
        string_to_write_to_file;

#endif
    if (initialized_updates_) updates_.push_back(update);
    
    //append the update to file
    write(string_to_write_to_file);
}

void UnappliedUpdatesStore::popUpdate()
{
    assert(initialized_updates_);
    ++popped_;
    if (popped_ == updates_.size())
    {
        //we don't have any updates to write, so we're changing the Y into a N
        overwrite_beginning_flag('N');
    }
    else
    {
        //pop the first update and write the contents of the
        //new file to the stringstream

        std::stringstream ss;
        ss << 'Y' << std::endl;
        for (unsigned int i = popped_; i != updates_.size(); ++i)
        {
            ss << updates_[i] << std::endl;
        }

        //write the stringstream to file, after truncating the file first
#ifdef _WIN32
        CloseHandle(f_);
        if (!open_(2)) // 2 = open and truncate
            throw UpdaterException(UpdaterErrorCodes::CouldNotOpenFile, true)(
                    path_);
#else
        close(f_);
        if (!open_(2))
            throw UpdaterException(UpdaterErrorCodes::CouldNotOpenFile, true)(
                    path_);
#endif
        //write from the stringstream to the file
        write(ss.str());
    }

}

void UnappliedUpdatesStore::markUpdatesToBeApplied()
{
    overwrite_beginning_flag('Y');
}

void UnappliedUpdatesStore::markUpdatesToNeverBeApplied()
{
    overwrite_beginning_flag('Z');
}

void UnappliedUpdatesStore::overwrite_beginning_flag(char flag)
{
#ifdef _WIN32
    //move to beginning of file
    SetFilePointer(f_, 0, 0, FILE_BEGIN);

    //overwrite the first 3 bytes of the file to Y\r\n or Z\r\n
    std::string flag_str(1, flag); flag_str += "\r\n";

#else
    //move to the beginning of file
    lseek(f_, 0, SEEK_SET);

    //overwrite the first 2 bytes of the file to Y\n or Z\n
    //@TODO: OS X new line ending
    std::string flag_str(1, flag); flag_str += "\n";
#endif
    write(flag_str);
}

void UnappliedUpdatesStore::flush()
{
#ifdef _WIN32
    if (!FlushFileBuffers(f_))
    {
        throw UpdaterException(
            UpdaterErrorCodes::CouldNotWriteToFile, true)(path_);
    }
#else
    //we've opened the file with O_DIRECT, no flushing should be needed
#endif
}

void UnappliedUpdatesStore::write(std::string const &data)
{
#ifdef _WIN32
    DWORD bytes_written;
    if (!WriteFile(f_, data.c_str(), data.size(),
                &bytes_written, NULL) || bytes_written < data.size())
    {
        throw UpdaterException(UpdaterErrorCodes::CouldNotWriteToFile, true)(
                path_);
    }
    flush();
#else
    if ( ::write(f_, data.c_str(), data.size()) < data.size() )
    {
        throw UpdaterException(UpdaterErrorCodes::CouldNotWriteToFile, true)(
                path_);
    }
#endif
}


UnappliedUpdatesStore::~UnappliedUpdatesStore()
{
#ifdef _WIN32
    CloseHandle(f_);
#else
    close(f_);
#endif
}


#include	"UpdaterErrorCodes.hxx"
#include	<map>
#include	<string>
#include	<boost/variant/get.hpp>
#include	<boost/variant/variant.hpp>

#ifndef UPDATER_LIBCOMMON_WITHOUT_I18N
#include	<localization.h>
#else
#define _(X) (X)
#endif

typedef std::map<unsigned int, std::string> ErrorCodeToMessageType;
namespace
{
    static ErrorCodeToMessageType initialize_messages()
    {
        ErrorCodeToMessageType msgs;
        using namespace UpdaterErrorCodes;
        msgs[Ok] = _("No error");
       
        msgs[CouldNotStartScilab] = _("Could not start scilab");
       
        msgs[CouldNotGetTempPath] = 
            _("Could not find path to temporary directory");
        msgs[CouldNotGetAppdataDirectory] =
            _("Could not find path to appdata directory");
        msgs[CouldNotLocateHomeDirectory] =
            _("Could not locate home directory. Please set the HOME "
                    "env variable.");
        
        msgs[CouldNotOpenFile] = _("Couldn't open file %s");
        msgs[CouldNotCreateDirectory] = _("Couldn't create directory %s");
        msgs[CouldNotWriteToFile] = _("Couldn't write to file %s");
//        msgs[CouldNotGetFileExclusiveLock] = 
//            _("Couldn't get exclusive lock on file %s");

        msgs[VersionDeserializationFailed] = 
            _("Version deserialization failed");
        msgs[UpdateDeserializationFailed] = _("Update deserialization failed");

        msgs[ExtractingError] = _("Couldn't extract: %s");
        msgs[CouldNotDecompress_CouldNotSpawnStdinPipe] =
            _("Could not decompress: Could not spawn stdin pipe");
        msgs[CouldNotDecompress_CouldNotSpawnStdoutPipe] =
            _("Could not decompress: Could not spawn stdout pipe");
        msgs[CouldNotDecompress_CouldNotSpawnTarProcess] =
            _("Could not decompress: Could not spawn tar process");
        msgs[CouldNotDecompress_CouldNotWriteToTarStdin] =
            _("Could not decompress: Could not write to tar's stdin "
                    "(I've only written %d out of %d)");
        msgs[CouldNotDecompress_TarReturnedNonzeroExitCode] =
            _("Could not decompress: Tar returned non-zero exit code");

        msgs[NoPatchingAlgorithmsAvailable] = 
            _("No patching algorithms available");
        msgs[NoNewUpdates] = _("No new updates available");
        msgs[CouldNotGetVersionOfModule] = 
            _("Could not get version of module ``%s''");
        msgs[CouldNotApplyUpdate] = _("Could not apply patch using "
                "algorithm ``%s'' to file %s");

        msgs[CouldNotParseUpdatesFile_NoUpdateId] =
            _("Could not parse update file: No update id on line %d");
        msgs[CouldNotParseUpdatesFile_NoFilename] =
            _("Could not parse update file: No filename on line %d");
        msgs[CouldNotParseUpdatesFile_NoPatchFilename] = 
            _("Could not parse update file: No patching filename on line %d");

        msgs[HaveDownloadedUnappliedUpdates] = _(
            "You have downloaded updates which were not applied. Please apply "
            "downloaded updates before attempting to check for updates or "
            "download updates again.");

        msgs[ForwardCurlException] = "%s";
        return msgs;
    }
}

//@TODO: fix inconsistent dll linkage warning
std::string message_from_exception (UpdaterException const &ec)
{
    static ErrorCodeToMessageType const msgs = initialize_messages();

    ErrorCodeToMessageType::const_iterator it;
    if ( ( it = msgs.find(ec.getErrorCode()) ) != msgs.end())
    {
        std::size_t num_params = ec.getNumParams();
        std::string msg = it->second;
        for (std::size_t i = 0; i < num_params; ++i)
        {
            if (int const *pi = boost::get<int>(&ec.getParam(i)) )
            {
                msg = cpp_printf(msg, *pi);
            }
            else if (std::string const *ps = 
                    boost::get<std::string>(&ec.getParam(i)))
            {
                msg = cpp_printf(msg, *ps);
            }
        }
        if (!ec.getOsMessage().empty()) msg += " [" + ec.getOsMessage() + "]";
        return msg;
    }
    else
    {
        std::string const &os_message = ec.getOsMessage();
        if (!os_message.empty())
            return cpp_printf( cpp_printf(
                        _("Updater error %d [%s]"),
                        ec.getErrorCode()),
                        os_message);
        return cpp_printf(_("Updater error %d"), ec.getErrorCode());
    }
}

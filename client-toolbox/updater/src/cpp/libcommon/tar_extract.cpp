
#include	"tar_extract.hxx"
#include	<stdexcept>
#include	<cstdarg> //@TODO: std::size_t?
#include	<cstdio>
#include	"common.hxx"
#include	"UpdaterErrorCodes.hxx"


#ifdef _WIN32
#include	<Windows.h>
#endif

/* #ifndef _WIN32
 * #include	<libarchive/archive.h>
 * #include	<libarchive/archive_entry.h>
 * #endif
 */

/* #ifndef _WIN32
 * class TarExtract
 * 
 * {
 * public:
 *     TarExtract (
 *             std::string const &tar_data,
 *             std::string const &output_folder)
 *         : tar_data_(tar_data), output_folder_(output_folder),
 *         arch_(archive_read_new()), arch_write_(archive_write_disk_new())
 * 
 *     {
 *         //archive_read_support_compression_gzip(arch_);
 *         if (archive_read_support_format_tar(arch_)) error_rd();
 * 
 *         if (archive_read_open_memory(arch_, tar_data_.c_str(),
 *                     tar_data_.size())) error_rd();
 *     }
 * 
 *     ~TarExtract ();
 * 
 *     void extract();
 * private:
 *     TarExtract (TarExtract const &);
 *     TarExtract &operator= (TarExtract const&);
 *     TarExtract ();
 * 
 *     std::string const &tar_data_;
 *     std::string const &output_folder_;
 *     struct archive *arch_, *arch_write_;
 * 
 *     void error_rd() const
 *     {
 *         throw UpdaterException(UpdaterErrorCodes::ExtractingError)(
 *                 archive_error_string(arch_));
 *     }
 *     void error_wr() const
 *     {
 *         throw UpdaterException(UpdaterErrorCodes::ExtractingError)(
 *                 archive_error_string(arch_write_));
 *     }
 * };
 * 
 * void TarExtract::extract ()
 * {
 *     struct archive_entry *entry;
 *     int err;
 *     for (;;)
 *     {
 *         err = archive_read_next_header(arch_, &entry);
 * 
 *         //done extracting files
 *         if (err == ARCHIVE_EOF) break;
 * 
 *         //error extracting next file's header
 *         if (err != ARCHIVE_OK) error_rd();
 * 
 *         //extract file in entry
 *         if (archive_write_header(arch_write_, entry) != ARCHIVE_OK)
 *             error_wr();
 * 
 *         //copy file to disk
 *         const void *buff;
 *         size_t size;
 *         off_t offset;
 *         for (;;)
 *         {
 *             err = archive_read_data_block(arch_, &buff, &size, &offset);
 *             if (err == ARCHIVE_EOF) break;
 * 
 *             if (err != ARCHIVE_OK) error_rd();
 * 
 *             err = archive_write_data_block(arch_write_, buff, size, offset);
 *             if (err != ARCHIVE_OK)
 *                 error_wr();
 *         }
 * 
 * 
 *         if (archive_write_finish_entry(arch_write_) != ARCHIVE_OK)
 *             error_wr();
 * 
 *     }
 * }
 * 
 * ~TarExtract ()
 * {
 *     archive_read_finish(arch_);
 *     archive_write_finish(arch_write_);
 * }
 * #endif
 */

void tar_extract (
        std::string const &tar_data,
        std::string const &output_folder)
{
    /* #ifndef _WIN32
 *     TarExtract tarExtract(tar_data, output_folder);
 *     tarExtract.extract();
 * #else
 * #endif
 */

    //@TODO: FOR WINDOWS, USE CreateProcess instead, so we can change the cwd
    //for tar.exe.
    //@TODO: escape tmp_out_
    //@TODO: In the future, reimplement this using libarchive

#if defined(_WIN32)
//    FILE *tarh = _popen(( "tar.exe -xC "+tmp_out_ ).c_str(), "wb");


//create pipes for communicating with tar.exe




    HANDLE tarInputPipeRd=NULL, tarInputPipeWr=NULL,
        tarOutputPipeRd=NULL, tarOutputPipeWr=NULL;
    PROCESS_INFORMATION tarh;
    ZeroMemory(&tarh, sizeof(tarh));

    try
    {
        SECURITY_ATTRIBUTES pipe_security_attributes;
        pipe_security_attributes.nLength = sizeof(SECURITY_ATTRIBUTES);
        pipe_security_attributes.bInheritHandle = TRUE;
        pipe_security_attributes.lpSecurityDescriptor = NULL;

//        sciprint("Creating first pipe\n");
        if (!CreatePipe(&tarInputPipeRd, &tarInputPipeWr,
                    &pipe_security_attributes, 0))
        {
            throw UpdaterException(
                UpdaterErrorCodes::CouldNotDecompress_CouldNotSpawnStdinPipe,
                true);
        }

//        sciprint("Creating second pipe\n");
        //@TODO: rtfm on what an error here means
        if (! SetHandleInformation(tarInputPipeWr, HANDLE_FLAG_INHERIT, 0) )
        {
            throw std::runtime_error("...");
        }
        
//        sciprint("A1\n");
        if (!CreatePipe(&tarOutputPipeRd, &tarOutputPipeWr,
                    &pipe_security_attributes, 0))
        {
            throw UpdaterException(
                UpdaterErrorCodes::CouldNotDecompress_CouldNotSpawnStdoutPipe,
                true);
        }
        
//        sciprint("A2\n");
        //@TODO: rtfm on what an error here means
        if (! SetHandleInformation(tarOutputPipeRd, HANDLE_FLAG_INHERIT, 0) )
        {
            throw std::runtime_error("...");
        }

        STARTUPINFO startup_info;
        ZeroMemory(&startup_info, sizeof(startup_info));
        startup_info.cb = sizeof(STARTUPINFO);
        startup_info.dwFlags = STARTF_USESHOWWINDOW|STARTF_USESTDHANDLES;
        startup_info.wShowWindow = SW_HIDE;
        startup_info.hStdInput = tarInputPipeRd;
        startup_info.hStdOutput = tarOutputPipeWr;
        startup_info.hStdError = tarOutputPipeWr;

//        sciprint("Spawning process\n");
        //@TODO: Use complete path and ship with tar.exe, not everyone has
        //UnxUtils and PATH properly set.
        if (!CreateProcessA(NULL, "tar.exe xj", NULL, NULL, TRUE, 0, NULL,
                    output_folder.c_str(), &startup_info, &tarh))
/*         if (!CreateProcessA("D:\\bin\\wbin\\writeto.exe",
 *                     "D:\\bin\\wbin\\writeto.exe C:/Users/stefan/Desktop/___.txt",
 *                     NULL, NULL, TRUE, 0, NULL,
 *                     output_folder.c_str(), &startup_info, &tarh))
 */
        {
            throw UpdaterException(
                UpdaterErrorCodes::CouldNotDecompress_CouldNotSpawnTarProcess,
                true);
        }
//        sciprint("Spawned process\n");
        
        //write tar data to tar.exe's stdin
        DWORD numBytesWritten;
        if (!WriteFile(tarInputPipeWr, tar_data.c_str(), tar_data.size(),
                    &numBytesWritten, NULL) || numBytesWritten < tar_data.size())
            throw UpdaterException(
                UpdaterErrorCodes::CouldNotDecompress_CouldNotSpawnTarProcess,
                true);
/*         sciprint("Before WriteFile()\n");
 *         std::size_t size = tar_data.size(), crtsize, offset=0;
 *         static std::size_t const WRITE_CHUNKS = 10;
 *         unsigned int i=1;
 *         while (size)
 *         {
 *             sciprint("Before iteration %d\n", i);
 *             if (size < WRITE_CHUNKS) crtsize = size;
 *             else crtsize = WRITE_CHUNKS;
 *             if (!WriteFile(tarInputPipeWr, tar_data.c_str()+offset, crtsize,
 *                         &numBytesWritten, NULL) || numBytesWritten < crtsize)
 *             {
 *                 throw WindowsException( cpp_printf(
 *                             _("Couldn\'t decompress: %s"),
 *                             _("couldn't write to tar process' stdin")));
 *             }
 *             size -= crtsize;
 *             offset += crtsize;
 *             sciprint("After iteration %d\n", i++);
 *         }
 *         sciprint("After WriteFile()\n");
 */
        //cause EOF on client's stdin
        CloseHandle(tarInputPipeWr); tarInputPipeWr = NULL;

        //@TODO: get the return value of tar.exe somehow
        WaitForSingleObject(tarh.hProcess, INFINITE);

        //@TODO: ReadFile on tarOutputPipeRd and write to tarlog
    }
    catch (...)
    {
        //@TODO: does CloseHandle(NULL) work?
        CloseHandle(tarInputPipeRd); CloseHandle(tarInputPipeWr);
        CloseHandle(tarOutputPipeRd); CloseHandle(tarOutputPipeWr);
        CloseHandle(tarh.hProcess); CloseHandle(tarh.hThread);
        throw;
    }
    CloseHandle(tarInputPipeRd); CloseHandle(tarInputPipeWr);
    CloseHandle(tarOutputPipeRd); CloseHandle(tarOutputPipeWr);
    CloseHandle(tarh.hProcess); CloseHandle(tarh.hThread);



#else
    FILE *tarh = popen(( "tar xjC "+output_folder ).c_str(), "w");

    if (!tarh)
        throw UpdaterException(
            UpdaterErrorCodes::CouldNotDecompress_CouldNotSpawnTarProcess,
            true);

    //I don't think std::string is guaranteed to be contiguous
    std::vector<char> tar_data_v(tar_data.begin(), tar_data.end());

    std::size_t bytes_written = 0;
    static const std::size_t BLOCK_SIZE = 512;
    while (bytes_written < tar_data.size())
    {
        std::size_t to_write = BLOCK_SIZE, written_now;
        if (tar_data.size() - bytes_written < to_write)
            to_write = tar_data_v.size() - bytes_written;

        if ((written_now = fwrite(&tar_data_v[bytes_written],
                        to_write, 1, tarh)) < 1)
        {
            throw UpdaterException(
                    UpdaterErrorCodes::CouldNotDecompress_CouldNotWriteToTarStdin,
                    true)(bytes_written)(tar_data_v.size());
        }
        bytes_written += written_now;
    }
    
    if (pclose(tarh))
        throw UpdaterException(
            UpdaterErrorCodes::CouldNotDecompress_TarReturnedNonzeroExitCode,
            true);
#endif
}



#include	"Version.hxx"
#include	"common.hxx"
#include	<stdexcept>
#include	<cassert>
#include	<sstream>
#include	<iomanip>
#include	"UpdaterErrorCodes.hxx"

////////////////////////////////////////////////////////////
//Implementation of class Version//
////////////////////////////////////////////////////////////

void Version::serializeInto (std::ostream &ss) const

{
    assert(string_.find('|') == std::string::npos);

    ss << major_ << '|' << minor_ << '|' << maintenance_ <<
        '|' << revision_ << '|' << string_ << '|';
}

Version Version::unserializeFrom (std::istream &ss)

{
    Version ret;

    if (!(ss >> ret.major_) || ss.peek() != '|')
        throw UpdaterException(UpdaterErrorCodes::VersionDeserializationFailed);
    ss.ignore(1);
   
    if (!(ss >> ret.minor_) || ss.peek() != '|')
        throw UpdaterException(UpdaterErrorCodes::VersionDeserializationFailed);
    ss.ignore(1);
   
    if (!(ss >> ret.maintenance_) || ss.peek() != '|')
        throw UpdaterException(UpdaterErrorCodes::VersionDeserializationFailed);
    ss.ignore(1);
   
    if (!(ss >> ret.revision_) || ss.peek() != '|')
        throw UpdaterException(UpdaterErrorCodes::VersionDeserializationFailed);
    ss.ignore(1);
    
    if (!std::getline(ss, ret.string_, '|'))
        throw UpdaterException(UpdaterErrorCodes::VersionDeserializationFailed);

    return ret;
}

Version::operator std::string () const
{
    std::stringstream ss;
    ss << major_ << '.' << 
        std::setfill('0') << minor_ <<
        '.' << std::setw(1) << maintenance_;
    if (revision_)
        ss << std::setw(2) << '.' << revision_;
    if (!string_.empty())
        ss << " (" << string_ << ')';
    return ss.str();
}

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

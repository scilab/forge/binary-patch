function UpdaterBuildCppSrc
    CWD = get_absolute_file_path('builder_cpp.sce') 
    exec(CWD + '/../../config.sce');

    UPDATER_CFLAGS = ..
        '-I' + CWD + '../../includes ' + ..
        '-I' + BOOST_INC_DIR + ' ' + ..
        '-DUPDATER_BUILDING_SRC ' + ..
        '-DUPDATER_PLATFORM_STRING=' + UPDATER_PLATFORM_STRING;
    LIB_PATH = CWD + '/../../libs/';

    //@TODO: probably a good idea to set a define equal to the path to the
    //toolbox

    // Build common stuff
    //TODO: curl_adaptor in common?
    //Note: If moving any cpp file into another dll, we also have to change the
    //macro used in its .hxx file for dllexport/dllimport
    COMMON_OBJS = [ ..
        'common' ..
        'tar_extract' ..
        'TemporaryDirectory' ..
        'Update' ..
        'Version' ..
        'UnappliedUpdatesStore' ..
        'UpdaterErrorCodes' ..
    ];
        //'WindowsException' ..
    COMMON_LINK = [];

    //@TODO: This is only needed for windows >= xp
    //This is needed for calling SHGetFolderPathA() to get appdata folder
    //@TODO: maybe some scilab function to get the appdata folder?
    if getos() == 'Windows' then
        COMMON_LINK = [COMMON_LINK 'shell32'];
    end

    disp('!!!! BUILDING LIBCOMMON !!!!');
    tbx_build_src( ..
        COMMON_OBJS, ..
        COMMON_OBJS + '.cpp', 'c', ..
        CWD+'/libcommon', COMMON_LINK, '', ..
        UPDATER_CFLAGS+' -DUPDATER_BUILDING_COMMON_SRC', '', '', 'common');

    //it would be great if we could somehow tell tbx_build_src to not link with
    //scilab dll-s (so-s)
    disp('!!!! BUILDING LIBCOMMON (WITHOUT I18N) !!!!');
    tbx_build_src( ..
        COMMON_OBJS, ..
        COMMON_OBJS + '.cpp', 'c', ..
        CWD+'/libcommon', COMMON_LINK, '', ..
        UPDATER_CFLAGS+' -DUPDATER_BUILDING_COMMON_SRC ' + ..
            '-DUPDATER_LIBCOMMON_WITHOUT_I18N', '', '', 'common_no_i18n');

   
   // Build the patching algorithms manager (manages all patching algorithms)
    // and build all the patching algorithms (bspatch, text diff etc.)
    MANAGER_OBJS = [ ..
        PATCHING_ALGORITHMS + 'PatchingAlgorithm', ..
        'PatchingAlgorithmsManager' ..
        'bspatch' ..
    ];
    MANAGER_LIBS = [ ..
        '../libcommon/libcommon' ..
    ];
    if getos() == 'Windows' then
        MANAGER_LIBS = [MANAGER_LIBS 'WSock32'];
    end

    disp('!!!! BUILDING PATCHING_ALGORITHMS_MANAGER !!!!');
    tbx_build_src( ..
        MANAGER_OBJS, MANAGER_OBJS+'.cpp', ..
        'c', CWD+'/libpatching_algorithms_manager', ..
        MANAGER_LIBS, '', ..
        UPDATER_CFLAGS + ..
            ' -DUPDATER_BUILDING_PATCHING_ALGORITHMS_MANAGER_SRC', ..
        '', '', 'patching_algorithms_manager');

    
    //Build everything else
    OBJS = ['CurlAdaptor', ..
            'CheckForUpdates', ..
            'DownloadUpdates', ..
            'UpdaterThread' ..
            ];

    
    LIBS = [ ..
        LIB_PATH + 'libcurl', ..
        SCI+'/bin/LibScilab' ..
        '../libcommon/libcommon' ..
        '../libpatching_algorithms_manager/libpatching_algorithms_manager' ..
    ];

    LDFLAGS = '';
    if COMPILER == 'gcc' then
        LDFLAGS = LDFLAGS + ' -lboost_thread';
    end

    SRCS = OBJS + '.cpp';
    
    disp('!!!! BUILDING LIBUPDATER_IMPL !!!!');
    tbx_build_src(OBJS, SRCS, 'c', ..
        CWD+'libupdater_impl/', LIBS,  ..
        UPDATER_BOOST_LDFLAGS+' '+LDFLAGS, ..
        UPDATER_CFLAGS + ' -DUPDATER_BUILDING_IMPL_SRC', ..
        '', '', 'updater_impl');

    //tbx_build_src(?check_for_updates@@YA?AV?$vector@UUpdate@@V?$allocator@UUpdate@@@std@@@std@@XZ

endfunction

UpdaterBuildCppSrc;
clear UpdaterBuildCppSrc;


//tbx_build_src( ..
    //['send_whole_file_updater', ..
    //'text_updater', ..
    //'xdelta_updater', ..
    ////'curl_adaptor', ..
    //'check_for_updates'], ..
    //['send_whole_file_updater.cpp', ..
    //'text_updater.cpp', ..
    //'xdelta_updater.cpp', ..
    //'curl_adaptor.cpp', ..
    ////'check_for_updates.cpp' ], ..
    //'c', get_absolute_file_path('builder_cpp.sce'), 'libcurl', '', UPDATER_CFLAGS);

//tbx_build_src( ..
//    ['check_for_updates', 'curl_adaptor'], ['check_for_updates.cpp', ..
//    'curl_adaptor.cpp'], ..
//    'c', get_absolute_file_path('builder_cpp.sce'), 'libcurl', '', UPDATER_CFLAGS);


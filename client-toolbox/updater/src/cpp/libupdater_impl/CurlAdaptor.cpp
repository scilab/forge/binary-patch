#include	"CurlAdaptor.hxx"

bool CurlEasy::initialized_libcurl = false;

void CurlEasy::initialize_libcurl ()
{
    if (!initialized_libcurl)
    {
        curl_global_init(CURL_GLOBAL_ALL);
        initialized_libcurl = true;
    }
}

std::string CurlEasy::urlencode (std::string const &str)
{
    static const std::string dontescape = "_-~`";
    static const char hex[] = "0123456789abcdef";
    std::string ret;
    ret.reserve(str.size());
    for (std::string::const_iterator it = str.begin(); it != str.end(); ++it)
    {
        if ( !std::isalnum(*it) && dontescape.find(*it) == std::string::npos)
        {
            ret += '%';
            ret += hex[*it / 16];
            ret += hex[*it % 16];
        }
        else
            ret += *it;
    }
    return ret;
}

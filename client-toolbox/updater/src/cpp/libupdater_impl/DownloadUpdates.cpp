
#include	"DownloadUpdates.hxx"
#include	<vector>
#include	<sstream>
//#include	<stdio.h> // popen
#include	<stdexcept>
#include	"CurlAdaptor.hxx"
#include	"PatchingAlgorithmsManager.hxx"
#include	"common.hxx"
#include	"tar_extract.hxx"
#include	"UpdaterErrorCodes.hxx"
#include	"TemporaryDirectory.hxx"
#include	"UnappliedUpdatesStore.hxx"

//#include	<sciprint.h> //@TODO: remove

class DownloadUpdates : public CurlAdaptor<DownloadUpdates>
{
public:
    DownloadUpdates(UpdatesVector const &updates, std::string const &tmp_out)
        : updates_(updates), tmp_out_(tmp_out)
    {
        if (!PatchingAlgorithmsManager::getInstance().areAny())
        {
            throw UpdaterException(
                    UpdaterErrorCodes::NoPatchingAlgorithmsAvailable);
        }

        if (updates.empty())
        {
            throw UpdaterException(UpdaterErrorCodes::NoNewUpdates);
        }

        //serialize the vector of updates
        std::stringstream ss;
        accumulate(updates_.begin(), updates_.end(), ss,
                &serializer_accumulator<Update>);

        ss << ':';

        //serialize the vector of updaters
        //Example: 1|2|3||
        PatchingAlgorithmsManager::patching_algorithms_map const &
            patching_algos = PatchingAlgorithmsManager::getInstance().getAll();

        //@TODO: boost::interprocess::map for binary compatibility
        for (PatchingAlgorithmsManager::patching_algorithms_map:: \
                const_iterator it = patching_algos.begin();
                it != patching_algos.end(); ++it)
        {
            ss << it->first << '|';
        }
        ss << '|';

        setPostData( "v=1&p=" + CurlEasy::urlencode(ss.str()) +
                "&pl=" + CurlEasy::urlencode( BOOST_PP_STRINGIZE(
                        UPDATER_PLATFORM_STRING )));
        setUserAgent( "Scilab-Updater/1.0" );
    }

    void downloadUpdates()
    {
//        sciprint("Sending request...\n");
        curlPerform(download_updates_url);
    }
private:
    DownloadUpdates();
    DownloadUpdates(DownloadUpdates const&);
    DownloadUpdates &operator= (DownloadUpdates const &);

    UpdatesVector updates_;
    std::string tmp_out_;
    static std::string const download_updates_url;

    friend class CurlAdaptor<DownloadUpdates>;

    //called automatically by CurlAdaptor when we've finished
    //downloading updates from /download_updates
    void onDownloadComplete (std::string const &data)
    {
//        sciprint("Got tar file.\n");
        tar_extract(data, tmp_out_);
    }
};

std::string const DownloadUpdates::download_updates_url =
        "http://127.0.0.1/download_updates";

void download_updates (UpdatesVector const &updates)
{
    //create a temporary directory to store downloaded updates
    TemporaryDirectory tmp_dir;

    //download the updates
    DownloadUpdates download_updates(updates, tmp_dir.getPath());
    download_updates.downloadUpdates();

    //use the unapplied_updates text file to write the path to the update to it
    UnappliedUpdatesStore unapplied_updates_store;
    if (!unapplied_updates_store.open(true))
        throw UpdaterException(
                UpdaterErrorCodes::CouldNotOpenFile)(
                    unapplied_updates_store.getPath());
    unapplied_updates_store.appendUpdate(tmp_dir.getPath());

    //everything else went fine, keep the temp directory
    tmp_dir.keepIt();
}

void throw_if_downloaded_unapplied_updates()
{
    UnappliedUpdatesStore unapplied_updates_store;
    if (!unapplied_updates_store.open())
    {
        throw UpdaterException(UpdaterErrorCodes::CouldNotOpenFile)
            (unapplied_updates_store.getPath());
    }
    if (unapplied_updates_store.areUnappliedUpdates(false))
    {
        throw UpdaterException(
                UpdaterErrorCodes::HaveDownloadedUnappliedUpdates);
    }
}

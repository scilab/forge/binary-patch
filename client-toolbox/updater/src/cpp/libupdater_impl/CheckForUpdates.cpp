
#include	"CheckForUpdates.hxx"

//@TODO: maybe observer so someone can register whenever updates are available?
//@TODO: use interprocess containers for binary compatibility
//@TODO: add an allocator template parameter everywhere


std::string const CheckForUpdates::check_for_updates_url =
    "http://127.0.0.1/check_for_updates";

//Get a vector of available updates.
//\throw CurlException Whenever connection to the updater server has failed
//\throw UpdaterException Failed to get a list of modules, a version of a
//  certain module, failed to communicate properly with the server etc.
//\return Vector of Update-s.
/* UpdatesVector check_for_updates ()
 * {
 *     CheckForUpdates updateChecker;
 *     updateChecker.checkForUpdates();
 *     return updateChecker.getAvailableUpdatesVector();
 * }
 */

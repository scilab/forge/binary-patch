#include "TextPatchingAlgorithm.hxx"
#include "PatchingAlgorithmsManager.hxx"

#ifdef _WIN32
#include	<Windows.h> // _popen()
#else
#include	<cstdio> // std::popen
#endif

unsigned int TextPatchingAlgorithm::ALGORITHM_ID = 2;

bool TextPatchingAlgorithm::applyUpdate (std::string const &filename,
		std::string const &patchfile)
{
    //@TODO: deduce this in some way
	static const std::string patch_binary = "D:/bin/wbin/patch.exe";

	//for now, we'll just call diff
	//(available on windows via unxutils)
	
    //@TODO: escaping
    std::string cmdline = patch_binary + " -e " + filename + " " + patchfile;

#	ifdef _WIN32
    FILE *fh = _popen(cmdline.c_str(), "r");
#	else
    FILE *fh = popen(cmdline.c_str(), "r");
#	endif

    char buf[1024];
    while (fread(buf, 1, 1024, fh) > 0);

// patch.exe returns 0 on success
#ifdef _WIN32
    return _pclose(fh) == 0;
#else
    return pclose(fh) == 0;
#endif
}

UPDATER_PATCHING_ALGORITHMS_MANAGER_SRC_EXPORT_IMPORT 
PatchingAlgorithm *create_text_palg () { return new TextPatchingAlgorithm; }

// register
namespace
{
	static bool text_palg_dummy_val = 
		PatchingAlgorithmsManager::getInstance().registerPatchingAlgorithm(
		TextPatchingAlgorithm::ALGORITHM_ID, &create_text_palg);
}

#include	"SendWholeFilePatchingAlgorithm.hxx"
#include	"PatchingAlgorithmsManager.hxx"

#if defined(_WIN32)
#include <Windows.h>
#elif defined(__APPLE__)
#include <copyfile.h>
#else
#include	<unistd.h>
#include	<sys/types.h>
#include	<sys/stat.h>
#include	<fcntl.h>
#endif

unsigned int SendWholeFilePatchingAlgorithm::ALGORITHM_ID = 255;

bool SendWholeFilePatchingAlgorithm::applyUpdate (
        std::string const &filename,
		std::string const &patchfile)
{
#	if defined(_WIN32)
	return CopyFileA(patchfile.c_str(), filename.c_str(), FALSE)==TRUE;
#	elif defined(__APPLE__)
	return copyfile(patchfile.c_str(),
            filename.c_str(), NULL, COPYFILE_ALL) == 0;
#   else
    //@TODO: bug report in boost's libs/filesystem/v3/src/operations.cpp @
    //POSIX HELPERS : copy_file_api (missed a close() after stat() failure)

    int from_fd = -1, to_fd = -1;

    if ( (from_fd = open(patchfile.c_str(), O_RDONLY)) == -1)
    {
        return false;
    }

    if ( (to_fd = open(patchfile.c_str(), O_WRONLY|O_TRUNC|O_CREAT)) == -1)
    {
        close(from_fd);
        return false;
    }

    ssize_t size_read, size_written, size;
    char buf[32768];

    while ( (size_read = read(from_fd, buf, 32768)) > 0)
    {
        size_written = 0;
        while (size_written < size_read)
        {
            if ( (size = write(to_fd, buf+size_written,
                            size_read-size_written)) < 0)
            {
                //write to output file failed
                close(from_fd);
                close(to_fd);
                return false;
            }
            size_written += size;
        }
    }

    close(from_fd);
    close(to_fd);
    return size_read >= 0;

#	endif
}

UPDATER_PATCHING_ALGORITHMS_MANAGER_SRC_EXPORT_IMPORT
PatchingAlgorithm *create_send_whole_file_palg()
    { return new SendWholeFilePatchingAlgorithm; }

//register the patching algorithm to the manager
namespace
{
	static bool send_whole_file_palg_dummy = 
		PatchingAlgorithmsManager::getInstance().registerPatchingAlgorithm(
		SendWholeFilePatchingAlgorithm::ALGORITHM_ID,
		&create_send_whole_file_palg);
}

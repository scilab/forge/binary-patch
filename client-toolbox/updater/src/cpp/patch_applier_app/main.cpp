//NOTE: THIS NEEDS TO LINK TO: libcommon and libpatching_algorithms_manager
//@TODO: Problem: this links with some DLLs, how will it be able to update
//those while they're in use?
//@TODO: we must make sure all instances of scilab are closed, not just the
//current one. so, how do we close all instances of scilab?

#ifdef _WIN32
#include	<Windows.h>
#else
#include	<sys/types.h>
#include	<unistd.h>
#endif

#include	<exception>
#include	<stdexcept>
#include	<string>
#include	<vector>
#include	<fstream>
#include	<sstream>
#include	<map>
#include	<iostream>
#include	<limits>
#include	"PatchingAlgorithm.hxx"
#include	"PatchingAlgorithmsManager.hxx"
#include	"common.hxx"
#include	"UpdaterErrorCodes.hxx"


#include	"UnappliedUpdatesStore.hxx"

void report_updating_state (std::string const &state_file, int state)
{
    std::ofstream ofh(state_file.c_str(), std::ios::trunc);
    if (!ofh) throw UpdaterException(UpdaterErrorCodes::CouldNotOpenFile)(
                state_file);
    
    if (! (ofh << state) ) throw UpdaterException(
            UpdaterErrorCodes::CouldNotWriteToFile)(state_file);
}

void apply_one_update(std::string const &path_to_update,
        std::string const &path_to_scilab)
{
    //after how many iterations should we update the state.txt file?
    static const unsigned int UPDATE_STATE_FILE_PERIOD = 1;
    std::string state_file = path_to_update + "/state.txt";
    int state;
    std::ifstream ifh(state_file.c_str(), std::ios::in);
    if (!ifh || !(ifh >> state))
    {
        state = 0;
    }
    ifh.close();
    if (state == -1) return;

    std::ifstream uf( (path_to_update + "/updates.txt").c_str(),
            std::ios::in);
    if (!uf) throw UpdaterException(UpdaterErrorCodes::CouldNotOpenFile)
            (path_to_update+"/updates.txt");

    int i=0;

    std::map<unsigned int, PatchingAlgorithm*> algorithm_objects;

    for (std::string crtline; std::getline(uf, crtline); ++i)
    {
        if (i < state) continue;

        unsigned int algorithm_id;
        std::string filename, patch_filename;
        std::stringstream ss(crtline);

        if (!(ss >> algorithm_id)) 
            throw UpdaterException(
                UpdaterErrorCodes::CouldNotParseUpdatesFile_NoUpdateId)(
                i+1);

        ss.ignore((std::numeric_limits<unsigned int>::max)(), ':');

        if (!std::getline(ss, filename, ':')) 
            throw UpdaterException(
                UpdaterErrorCodes::CouldNotParseUpdatesFile_NoFilename)(
                i+1);
        if (!std::getline(ss, patch_filename, ':')) 
            throw UpdaterException(
                UpdaterErrorCodes::CouldNotParseUpdatesFile_NoPatchFilename)(
                i+1);


        //create an algorithm object of the required id if we don't have one
        if (algorithm_objects.find(algorithm_id) == algorithm_objects.end())
            algorithm_objects[algorithm_id] =
                PatchingAlgorithmsManager::getInstance(). \
                    create_instance(algorithm_id);

        //apply the patch
        if (!algorithm_objects[algorithm_id]->applyUpdate(
                   path_to_scilab + "/" + filename,
                   path_to_update + "/" + patch_filename))
            throw UpdaterException(UpdaterErrorCodes::CouldNotApplyUpdate)
                (algorithm_objects[algorithm_id]->getPatchingAlgorithmName())
                (filename);

        //report the entries in updates.txt that have been treated.
        //this is useful when an update procedure is stopped and needs to
        //resume.
        if ( (i-state+1)%UPDATE_STATE_FILE_PERIOD == 0)
        {
            report_updating_state(state_file, i+1);
        }
    }
    //report the special state value -1 into the state file, meaning that all
    //the entries in updates.txt have been treated
    report_updating_state(state_file, -1);
}

void apply_updates(std::string const &path_to_scilab_binary,
        std::string const &path_to_scilab)
{

    UnappliedUpdatesStore updates_store;
    if (!updates_store.open(true))
        throw UpdaterException(UpdaterErrorCodes::CouldNotOpenFile)(
                updates_store.getPath());
    
    std::vector<std::string> const &updates = updates_store.getUpdatePaths();

    for (std::vector<std::string>::const_iterator it = updates.begin();
            it != updates.end(); ++it)
    {
        apply_one_update(*it, path_to_scilab);
        //remove the update that was just treated from the unapplied_updates
        //file
        updates_store.popUpdate();
        //@TODO: delete the path to this temporary update
    }

    //Respawn scilab
#ifdef _WIN32
    STARTUPINFO startup_info;
    PROCESS_INFORMATION scilab_proc;
    ZeroMemory(&startup_info, sizeof(startup_info));
    startup_info.cb = sizeof(STARTUPINFO);
    std::vector<char> path_to_scilab_binary_lpstr(
            path_to_scilab_binary.begin(),
            path_to_scilab_binary.end());
    path_to_scilab_binary_lpstr.push_back('\0');
    if (!CreateProcessA(NULL, &path_to_scilab_binary_lpstr[0],
                NULL, NULL, FALSE, 0,
                NULL, NULL, &startup_info, &scilab_proc))
        throw UpdaterException(UpdaterErrorCodes::CouldNotStartScilab, true);
    //@TODO: should I close this process' handles?
#else
    pid_t pid = fork();
    if (pid == -1)
        throw UpdaterException(UpdaterErrorCodes::CouldNotStartScilab, true);
    else if (pid == 0)
    {
        //child
        execl(path_to_scilab_binary.c_str(), path_to_scilab_binary.c_str(),
                NULL);
    }
    else
    {
        //parent
    }
    //@TODO: fork+execv
#endif
}

int main (int argc, char *argv[])
{
    try
    {
        //takes the path to scilab binary as param
		//throw std::runtime_error("Couldn't open file: fas/updates.txt");
        if (argc != 3)
            throw std::runtime_error("Invalid number of arguments.");
        apply_updates(argv[1], argv[2]);
    }
    catch (std::exception const &e)
    {
        std::cerr << e.what() << std::endl;
        //@TODO: remove or do something better for press any key to continue
        //std::cin.sync();
        //std::cin.get();
        return -1;
    }
    catch (UpdaterException const &e)
    {
        std::cerr << message_from_exception(e) << std::endl;
        return -1;
    }
    return 0;
}

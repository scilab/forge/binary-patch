
#include	"sci_download_updates.hxx"
#include	"DownloadUpdates.hxx"
#include	"CheckForUpdates.hxx"
#include	<sciprint.h>
#include	<api_scilab.h>
#include	<stack-c.h>
#include	<Scierror.h>
#include	<localization.h>
#include	"common.hxx"
#include	"TemporaryDirectory.hxx"
#include	"UnappliedUpdatesStore.hxx"
#include	"UpdaterThread.hxx"
#include	"UpdaterErrorCodes.hxx"


//@TODO: turn this into sci_download_all_updates,
//sci_download_updates should take a matrix returned by sci_check_for_updates
//as input
//@TODO: if we allow the user to only download certain updates, we have to also
//store a dependency tree in the client so we forcefully download the
//dependencies
extern "C" int sci_download_updates (char *fname)
{

    //@TODO: change this value to something bigger
    //this represents the number of seconds that have to pass since the last
    //check for updates for sci_download_updates to perform a recheck.
    static const unsigned int MINIMUM_AMOUNT_TO_FORCE_RECHECK = 30;

    CheckLhs(0,1);
    //@TODO: when it gets a param, use it
    CheckRhs(0,1);

    try
    {
        //we have downloaded, unapplied updates
        throw_if_downloaded_unapplied_updates();

        bool err = false;
        if (UpdaterThread::getInstance().isRunning())
        {
            UpdaterThread::ErrorCode ec;
            if ( (ec = UpdaterThread::getInstance(). \
                    forceUpdatesCheckAndDownloadAndWait()) != UpdaterThread::Ok)
            {
                Scierror(999, 
                    UpdaterThread::getInstance().getMessageFromErrorCode(ec). \
                    c_str());
                err = true;
            }
        }
        else
        {
            if (CheckForUpdates::getInstance().secondsFromLastUpdateCheck() >=
                    MINIMUM_AMOUNT_TO_FORCE_RECHECK)
            {
                CheckForUpdates::getInstance().check();
            }
            download_updates(
                    CheckForUpdates::getInstance().getAvailableUpdates());
        }
        if (!err)
        {
            sciprint("%s\n",
                _( "The updates have been successfully downloaded. "
                "Please apply_downloaded_updates to schedule updates to "
                "be applied on the next restart."));
        }
    }
    catch (UpdaterException const &e)
    {
        Scierror(999, message_from_exception(e).c_str());
    }
    catch (CurlException const &e)
    {
        Scierror(999, _("Error checking for updates: %s"), e.what());
    }

/*     sciprint(_("Downloading updates...\n"));
 *     std::string tmp_dir;
 *     try
 *     {
 * 
 *         //pick a temp dir
 * 
 * 
 *         TemporaryDirectory tmp_dir;
 *         download_updates(CheckForUpdates::getInstance().getAvailableUpdates(),
 *                 tmp_dir.getPath());
 *  
 *         //return the tmp_dir
 *         if ( createSingleString(pvApiCtx, Rhs + 1, tmp_dir.getPath().c_str()) )
 *             throw std::runtime_error( cpp_printf(_("Scilab error: %s"),
 *                         "createSingleString"));
 *         LhsVar(1) = Rhs + 1;
 *         tmp_dir.keepIt();
 *     }
 *     catch (std::runtime_error const &e)
 *     {
 *         Scierror(999, e.what());
 *     }
 */
    return 0;
}


#include	"sci_start_periodical_updates_checker.hxx"

#include	<sciprint.h>
#include	<api_scilab.h>
#include	<stack-c.h>
#include	<Scierror.h>

#include	"UpdaterThread.hxx"

extern "C" int sci_start_periodical_updates_checker (char *fname)
{
    CheckLhs(0,1);
    CheckRhs(0,0);

    UpdaterThread::ErrorCode ec;
    if ( (ec = UpdaterThread::getInstance().runPeriodicalUpdatesChecker()) !=
            UpdaterThread::Ok)
    {
        Scierror(999, 
            UpdaterThread::getInstance().getMessageFromErrorCode(ec).c_str());
    }
    else sciprint("Updater thread started.\n");
    return 0;
}

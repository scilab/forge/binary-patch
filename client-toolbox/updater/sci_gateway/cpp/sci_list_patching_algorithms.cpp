
#include	"sci_list_patching_algorithms.hxx"
#include	<sciprint.h>
#include	<api_scilab.h>
#include	<stack-c.h>
#include	<Scierror.h>
#include	<localization.h>
#include	<memory>
#include	"PatchingAlgorithmsManager.hxx"
#include	"PatchingAlgorithm.hxx"

extern "C" int sci_list_patching_algorithms (char *fname)
{
    CheckRhs(0,0);
    PatchingAlgorithmsManager::patching_algorithms_map const &algorithms =
        PatchingAlgorithmsManager::getInstance().getAll();

    typedef 
        PatchingAlgorithmsManager::patching_algorithms_map::const_iterator It;

    for (It it = algorithms.begin(); it != algorithms.end(); ++it)
    {
        std::auto_ptr<PatchingAlgorithm> alg(it->second());
        sciprint("%d [%s]\n", alg->getPatchingAlgorithmId(),
                alg->getPatchingAlgorithmName().c_str());
    }
    return 0;
}

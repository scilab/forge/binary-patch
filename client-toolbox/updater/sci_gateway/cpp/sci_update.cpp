#include "sci_update.hxx"
#include "updater_manager.hxx"
#include <sstream>
#include <iostream>
#include <curl/curl.h>
#include <ctime> // std::time
#include <fstream>
#include <map>
#include <string>
#include <cstdlib>
#include "api_scilab.h"
#include "gateway_export.hxx"

namespace
{
	std::size_t got_data(char *bufptr, size_t size, size_t nitems, void *userp);
}

#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

// this should normally spawn a new thread (or use a thread pool if one is available)
// as the updating process is supposed to be nonblocking for the app
UPDATER_C_EXPORT int sci_update (
	char *fname,unsigned long fname_len)
{
	
	static bool initialized_libcurl = false;

	if (!updater_manager::get_instance().are_any_updaters())
	{
		std::cerr << "The update could not be performed because there are "
			"no updaters available." << std::endl;
		return false;
	}

	//connect to the http server to see if any updates are available

	//make a list of all the available updaters
	std::stringstream available_updaters_ss;
	for (updater_manager::updaters_map::const_iterator it =
		updater_manager::get_instance().get_updaters().begin();
		it != updater_manager::get_instance().get_updaters().end(); ++it)
	{
		available_updaters_ss << it->first << '|';
	}
	std::cerr << "Available updaters: " << available_updaters_ss.str() << std::endl;
	std::cerr << "Checking for available updates..." << std::endl;

	if (!initialized_libcurl)
	{
		curl_global_init(CURL_GLOBAL_ALL);
		initialized_libcurl = true;
	}

	std::string const update_url("http://127.0.0.1/downloadupdates.php");
	std::stringstream url;
	//we will probably send a list of all the modules and their version via POST
	url << update_url << '?' << "updaters=" << available_updaters_ss.str()
		<< "&version=1.2.3";

	std::string returned_data;
	char error[CURL_ERROR_SIZE];

	//!\todo this will be wrapped in a class to make sure proper cleanup
	//! is performed (RTTI). if our code currently throws an exception at unexpected
	//! times, curl_easy_cleanup is never called.
	CURL *curl = curl_easy_init();
	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
	curl_easy_setopt(curl, CURLOPT_URL, url.str().c_str());
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &got_data);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &returned_data);
	curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, error);
	if (curl_easy_perform(curl) != 0)
	{
		std::cerr << "Curl error: " << error << std::endl;
		curl_easy_cleanup(curl);
		return false;
	}
	curl_easy_cleanup(curl);

	if (returned_data == "-")
	{
		std::cerr << "scilab is up-to-date." << std::endl;
	}
	else
	{
		static const std::string update_dir = "temp";

		std::stringstream archive_filename_ss;
		archive_filename_ss << update_dir << "/update-" << std::time(NULL) << ".tar";

#		ifdef _WIN32
		CreateDirectoryA(update_dir.c_str(), NULL);
#		else
		mkdir(update_dir.c_str());
#		endif

		//save and extract the downloaded archive
		{
			std::fstream fh(
				archive_filename_ss.str().c_str(),
				std::ios::trunc|std::ios::binary|std::ios::out);
			if (!fh.good()) return false;
			fh.write(returned_data.c_str(), returned_data.size());
			if (!fh) return false;
			fh.close();
		}
		//for this simple PoC, we'll just call tar
		//tar.exe can be found in unxutils
		if (std::system(
			("tar xf \""+archive_filename_ss.str()+"\" -C temp").c_str()
			) != 0)
		{
			return false;
		}

		//delete the tar
#		ifdef _WIN32
		DeleteFileA(archive_filename_ss.str().c_str());
#		else
		unlink(archive_filename_ss.str().c_str());
#		endif

		//! \todo a smart ptr will be required here (RTTI)
		//this is currently a clear error leak, we never even bother to delete

		std::map<unsigned int, updater*> updaters;

		//look in the text file which lists the updates to make
		std::ifstream fh(
			(update_dir+"/updates/updates.txt").c_str());
		if (!fh.good()) return false;

		//format of each line: updater_id:filename:patch_filename
		for (std::string line; std::getline(fh, line);)
		{
			unsigned int updater_id;
			std::string filename, patch_filename;

			std::stringstream ss(line);
			ss >> updater_id;
			ss.ignore((std::numeric_limits<unsigned int>::max)(), ':');
			std::getline(ss, filename, ':');
			std::getline(ss, patch_filename, ':');

			//create an updater if we don't have one
			if (updaters.find(updater_id) == updaters.end())
				updaters[updater_id] =
					updater_manager::get_instance().create_updater(updater_id);
			
			std::cout << "Attempting to patch \"" << filename << "\""
				<< " using patch file \""<<update_dir<<"/updates/"<<patch_filename
				<<"\" via updater " << updaters[updater_id]->get_updater_name()
				<< std::endl;

			//apply the patch
			if (!updaters[updater_id]->apply_update(filename,
				(update_dir+"/updates/"+patch_filename).c_str()))
				return false;
		}
	}
	return true;
}

namespace
{
	//append the data into the string
	std::size_t got_data(char *bufptr, size_t size, size_t nitems, void *userp)
	{
		if (size * nitems == 0) return 0;
		std::string &data = *reinterpret_cast<std::string*>(userp);
		data.append(bufptr, bufptr+size*nitems);
		return size*nitems;
	}
}

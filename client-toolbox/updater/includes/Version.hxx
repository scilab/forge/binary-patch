#ifndef  INCLUDED_COMMON_VERSION_HXX_INC
#define  INCLUDED_COMMON_VERSION_HXX_INC


#include	"gateway_export.hxx"
#include	<string>
#include	<istream>
#include	<ostream>

//@TODO: PROBLEM!!!! WHY DOESN'T THE BUILD SYSTEM KNOW WHAT TO REBUILD
//(RECOMPILE) PROPERLY. IT JUST USES THE OLD STUFF FOR SOME REASON. FIND
//OUT WTF IS WRONG WITH MSVC MAKEFILES
struct UPDATER_COMMON_SRC_EXPORT_IMPORT Version
{
    Version () { }
    Version (unsigned int major, unsigned int minor,
            unsigned int maintenance, unsigned int revision,
            std::string const &string)
        : major_(major), minor_(minor), maintenance_(maintenance),
        revision_(revision), string_(string) { }

    Version (Version const &v)
        : major_(v.major_), minor_(v.minor_), maintenance_(v.maintenance_),
        revision_(v.revision_), string_(v.string_) { }

    Version &operator= (Version const &v)
    {
        if (this == &v) return *this;
        major_ = v.major_;
        minor_ = v.minor_;
        maintenance_ = v.maintenance_;
        revision_ = v.revision_;
        string_ = v.string_;
        return *this;
    }

    unsigned int major_, minor_, maintenance_, revision_;
    std::string string_;

    operator std::string () const;

    void serializeInto (std::ostream &out) const;
    static Version unserializeFrom (std::istream &ss);
};

#endif   /* ----- #ifndef INCLUDED_COMMON_VERSION_HXX_INC  ----- */


#ifndef  SCI_APPLY_DOWNLOADED_UPDATES_HXX_INC
#define  SCI_APPLY_DOWNLOADED_UPDATES_HXX_INC


#include	"gateway_export.hxx"

extern "C" UPDATER_GATEWAY_EXPORT int sci_apply_downloaded_updates (char *fname);

#endif   /* ----- #ifndef SCI_APPLY_DOWNLOADED_UPDATES_HXX_INC  ----- */

#ifndef  INCLUDED_UPDATER_UPDATEREXCEPTION_HXX_INC
#define  INCLUDED_UPDATER_UPDATEREXCEPTION_HXX_INC

#include	<boost/variant/variant.hpp>
#include	<cassert>
#include	<vector>
#include	<string>
#ifdef _WIN32
#include	<Windows.h>
#else
#include	<cstring> //strerror
#include	<errno.h>
#endif

class UpdaterException
{
    public:
        typedef boost::variant<std::string, int> IntOrString;

        UpdaterException (unsigned int errorCode=0, bool withOsMessage = false)
            : ec_(errorCode), params_()
        {
            if (withOsMessage)
            {
#ifdef _WIN32
                LPSTR winErr;
                FormatMessageA(
                        FORMAT_MESSAGE_ALLOCATE_BUFFER |
                        FORMAT_MESSAGE_FROM_SYSTEM |
                        FORMAT_MESSAGE_IGNORE_INSERTS,
                        NULL, GetLastError(), 0, (LPSTR)&winErr, 0, NULL);

                try { os_message_.assign(winErr); } catch (...) {}
                LocalFree(winErr);

                // remove the newline and everything after it.
                std::string::size_type newline_pos;
                if ( (newline_pos = os_message_.find_first_of("\r\n")) 
                        != std::string::npos)
                {
                    os_message_.assign(os_message_.begin(),
                            os_message_.begin()+newline_pos);
                }
#else
                try { os_message_.assign( std::strerror(errno) ); }
                catch (...) { }
#endif
            }
        }
        UpdaterException (UpdaterException const &other) 
            : ec_(other.ec_), params_(other.params_),
            os_message_(other.os_message_) { }
        UpdaterException &operator= (UpdaterException const &rhs)
        {
            if (&rhs == this) return *this;

            ec_ = rhs.ec_;
            params_ = rhs.params_;
            os_message_ = rhs.os_message_;
            return *this;
        }

        UpdaterException &operator()(IntOrString const &param)
        {
            params_.push_back(param);
            return *this;
        }

        unsigned int getErrorCode() const throw() { return ec_; }

        IntOrString const &getParam(unsigned int idx) const throw()
        {
            assert(idx < params_.size());
            return params_[idx];
        }

        std::size_t getNumParams () const throw() { return params_.size(); }

        std::string getOsMessage () const throw() { return os_message_; }
    private:
        unsigned int ec_;
        std::vector<IntOrString> params_;
        std::string os_message_;
};

#endif   /* ----- #ifndef INCLUDED_UPDATER_UPDATEREXCEPTION_HXX_INC  ----- */

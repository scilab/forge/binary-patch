#ifndef  INCLUDED_COMMON_HXX_INC
#define  INCLUDED_COMMON_HXX_INC

#include	<sstream>
#include	<string>
#include	"gateway_export.hxx"

#include	"UpdaterException.hxx"

#ifndef UPDATER_PLATFORM_STRING
#error "Please define UPDATER_PLATFORM_STRING as one of: w86, w64" \
    "l86, l64, m."
#endif

UPDATER_COMMON_SRC_EXPORT_IMPORT std::string string_replace_first
    (std::string &string, std::string const &from, std::string const &to);

UPDATER_COMMON_SRC_EXPORT_IMPORT std::string string_replace_first
    (std::string const &string, std::string const &from, std::string const &to);

UPDATER_COMMON_SRC_EXPORT_IMPORT std::string cpp_printf
    (std::string const &format, std::string const &str);

UPDATER_COMMON_SRC_EXPORT_IMPORT std::string cpp_printf
    (std::string const &format, double d);

UPDATER_COMMON_SRC_EXPORT_IMPORT std::string cpp_printf
    (std::string const &format, int i);

UPDATER_COMMON_SRC_EXPORT_IMPORT std::string cpp_printf
    (std::string const &format, unsigned int i);

template <class T>
void
serializer_accumulator(
        std::stringstream &accumulator,
        T const &element)
{
    element.serializeInto(accumulator);
}

template <class InputIterator, class T, class BinaryOperation>
inline void accumulate (InputIterator start, InputIterator finish,
        T &init, BinaryOperation binary_op)

{
    for (; start != finish; ++start)
    {
        binary_op(init, *start);
    }
}

#endif   /* ----- #ifndef INCLUDED_COMMON_HXX_INC  ----- */

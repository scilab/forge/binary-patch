#ifndef  INCLUDED_SENDWHOLEFILEPATCHINGALGORITHM_HXX_INC
#define  INCLUDED_SENDWHOLEFILEPATCHINGALGORITHM_HXX_INC

#include	"PatchingAlgorithm.hxx"

//! A simple updater that works by simply sending the whole file.
class SendWholeFilePatchingAlgorithm : public PatchingAlgorithm
{
public:
    static unsigned int ALGORITHM_ID;
    bool applyUpdate (std::string const &filename,
            std::string const &patchfile);
    std::string getPatchingAlgorithmName () const 
        { return "Send whole file patching algorithm"; }
    unsigned int getPatchingAlgorithmId() const { return ALGORITHM_ID; }

    virtual ~SendWholeFilePatchingAlgorithm() { }
};

#endif

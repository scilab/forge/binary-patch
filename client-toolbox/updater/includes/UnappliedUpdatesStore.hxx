
#ifndef  INCLUDED_COMMON_UNAPPLIEDUPDATESSTORE_HXX_INC
#define  INCLUDED_COMMON_UNAPPLIEDUPDATESSTORE_HXX_INC

#include	<fstream>
#include	<string>
#include	<vector>
#include	"gateway_export.hxx"

#ifdef _WIN32
#include <Windows.h> // for HANDLE in UnappliedUpdatesStore's class def
#endif

/*
 * This class keeps track (using a hdd file) of the updates that need to be
 * applied to scilab, and whether they were requested to be applied or not (via
 * apply_downloaded_updates).
 * @TODO: unit tests for this class?
 * @TODO: deal with case when scilab's appdata dir doesn't exist
 */
class UPDATER_COMMON_SRC_EXPORT_IMPORT UnappliedUpdatesStore
{
public:
    UnappliedUpdatesStore ();

    ~UnappliedUpdatesStore();

    bool open(bool read_write=false)
    {
        return open_(read_write ? 1 : 0);
    }

    /* Tests if there are any updates that have not been applied yet but need
     * to be applied.
     * If onlyIfNeedToApply is true, then this function returns true if the
     * updates are also scheduled to be applied.
     * If onlyIfNeedToApply is false, this returns true if there are any
     * downloaded updates, whether they are scheduled to be applied or not.
     */
    bool areUnappliedUpdates(bool onlyIfNeedToApply=true);

    std::vector<std::string> const &getUpdatePaths();

    /*
     * Adds a new update into the unapplied updates file.
     */
    void appendUpdate(std::string const &update);

    /*
     * Removes the top update path from the unapplied updates file.
     * Note that this does not remove it from the vector, so getUpdatePaths()
     * will return the same thing after this call.
     * This function flushes the output immediatelly, so the disk write is
     * complete by the time it returns.
     */
    void popUpdate();

    void markUpdatesToBeApplied();

    void markUpdatesToNeverBeApplied();

    /* 
     * Returns the path to the unapplied updates file
     */
    std::string getPath() const { return path_; }
private:

    void overwrite_beginning_flag(char flag);

    /*
     * Flush the write buffer.
     */
    void flush();

    /*
     * Write data to file at the current seeked position.
     */
    void write(std::string const &data);
    
    UnappliedUpdatesStore (UnappliedUpdatesStore const &);
    UnappliedUpdatesStore &operator= (UnappliedUpdatesStore const&);
    
    /*
     * Opens a file handle. This function is used both internally and forwarded
     * by its public version, open().
     * \private
     * \param op 0=read, 1=readwrite, 2=readwrite+truncate
     */
    bool open_(unsigned int op);

#ifdef _WIN32
    HANDLE f_;
#else
    int f_;
#endif
    unsigned int popped_;
    std::vector<std::string> updates_;
    std::string path_;
    bool initialized_updates_;
};

#endif   /* ----- #ifndef INCLUDED_COMMON_UNAPPLIEDUPDATESSTORE_HXX_INC  ----- */

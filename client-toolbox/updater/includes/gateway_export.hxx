
#ifndef  INCLUDED_GATEWAY_EXPORT_HXX_INC
#define  INCLUDED_GATEWAY_EXPORT_HXX_INC

//#ifdef UPDATER_BUILDING_SRC
//#define UPDATER_SRC_EXPORT_IMPORT __declspec(dllexport) 
//#else

//@TODO: replace _MSC_VER with _WIN32? maybe mingw needs this too?
#ifdef _WIN32

    //only display the "should have a DLL interface" warning once
    #pragma warning( once : 4251 )

    #define UPDATER_GATEWAY_EXPORT extern "C" __declspec(dllexport) 

    #ifdef UPDATER_BUILDING_SRC
    #define UPDATER_SRC_EXPORT_IMPORT __declspec(dllexport)
    #else
    #define UPDATER_SRC_EXPORT_IMPORT __declspec(dllimport)
    #endif
    
    #ifdef UPDATER_BUILDING_COMMON_SRC
    #define UPDATER_COMMON_SRC_EXPORT_IMPORT __declspec(dllexport)
    #else
    #define UPDATER_COMMON_SRC_EXPORT_IMPORT __declspec(dllimport)
    #endif

    #ifdef UPDATER_BUILDING_PATCHING_ALGORITHMS_MANAGER_SRC
    #define UPDATER_PATCHING_ALGORITHMS_MANAGER_SRC_EXPORT_IMPORT \
            __declspec(dllexport)
    #else
    #define UPDATER_PATCHING_ALGORITHMS_MANAGER_SRC_EXPORT_IMPORT \
            __declspec(dllimport)
    #endif

    #ifdef UPDATER_BUILDING_IMPL_SRC
    #define UPDATER_IMPL_SRC_EXPORT_IMPORT __declspec(dllexport)
    #else
    #define UPDATER_IMPL_SRC_EXPORT_IMPORT __declspec(dllimport)
    #endif
#else
    #define UPDATER_GATEWAY_EXPORT
    #define UPDATER_SRC_EXPORT_IMPORT
    #define UPDATER_COMMON_SRC_EXPORT_IMPORT
    #define UPDATER_PATCHING_ALGORITHMS_MANAGER_SRC_EXPORT_IMPORT
    #define UPDATER_IMPL_SRC_EXPORT_IMPORT
#endif
    
#endif   /* ----- #ifndef INCLUDED_GATEWAY_EXPORT_HXX_INC  ----- */

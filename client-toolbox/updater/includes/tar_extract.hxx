#ifndef  INCLUDED_COMMON_TAR_EXTRACT_HXX_INC
#define  INCLUDED_COMMON_TAR_EXTRACT_HXX_INC

#include	"gateway_export.hxx"
#include	<string>

UPDATER_COMMON_SRC_EXPORT_IMPORT void tar_extract (
        std::string const &tar_data,
        std::string const &output_folder);

#endif   /* ----- #ifndef INCLUDED_COMMON_TAR_EXTRACT_HXX_INC  ----- */

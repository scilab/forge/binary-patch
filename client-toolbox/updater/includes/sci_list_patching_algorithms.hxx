
#ifndef  INCLUDED_UPDATER_SCI_LIST_PATCHING_ALGORITHMS_HXX_INC
#define  INCLUDED_UPDATER_SCI_LIST_PATCHING_ALGORITHMS_HXX_INC

#include	"gateway_export.hxx"

extern "C" UPDATER_GATEWAY_EXPORT int sci_list_patching_algorithms (char *fname);

#endif

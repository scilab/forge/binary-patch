#ifndef  INCLUDED_COMMON_WINDOWSEXCEPTION_HXX_INC
#define  INCLUDED_COMMON_WINDOWSEXCEPTION_HXX_INC

#include	<stdexcept>
#include	"gateway_export.hxx"
#include	<string>

#ifdef _WIN32

class UPDATER_COMMON_SRC_EXPORT_IMPORT WindowsException
: public std::runtime_error

{
    public:
        WindowsException (std::string const &msg)
            : std::runtime_error(formatWindowsMessage(msg)) { }
        virtual ~WindowsException() { }
    private:
        static inline std::string formatWindowsMessage (std::string const &msg);
};

#endif

#endif   /* ----- #ifndef INCLUDED_COMMON_WINDOWSEXCEPTION_HXX_INC  ----- */

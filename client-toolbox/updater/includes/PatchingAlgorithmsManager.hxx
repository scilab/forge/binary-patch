
#ifndef  INCLUDED_UPDATER_PATCHING_ALGORITHMS_MANAGER_HXX_INC
#define  INCLUDED_UPDATER_PATCHING_ALGORITHMS_MANAGER_HXX_INC

#include	"PatchingAlgorithm.hxx"
#include	"gateway_export.hxx"
#include	<map>
#include	<cassert>

//!A singleton class that keeps track of all the available 
//!patching algorithm types.
//!
//!Inspired from the Object Factory idiom from "Thinking in C++"
//!@TODO: std::map should be boost::interprocess::map
class UPDATER_PATCHING_ALGORITHMS_MANAGER_SRC_EXPORT_IMPORT 
    PatchingAlgorithmsManager
{
public:
	//singleton
	static PatchingAlgorithmsManager &getInstance()
	{
		static PatchingAlgorithmsManager inst;
		return inst;
	}

	typedef PatchingAlgorithm *(*create_patching_algorithm_callback)();
	typedef std::map<unsigned int, create_patching_algorithm_callback>
		patching_algorithms_map;

	bool registerPatchingAlgorithm (unsigned int patching_algorithm_id,
		create_patching_algorithm_callback patching_algorithm_creator)
	{ algs_[patching_algorithm_id] = patching_algorithm_creator; return true; }

	PatchingAlgorithm *create_instance (unsigned int patching_algorithm_id)
	{
		assert(algs_.find(patching_algorithm_id) != algs_.end());
		return algs_[patching_algorithm_id]();
	}
	
	patching_algorithms_map const &getAll() const
	{
		return algs_;
	}

	bool areAny() const
	{
		return !algs_.empty();
	}
private:
	patching_algorithms_map algs_;

	//singleton
	PatchingAlgorithmsManager() { }
	~PatchingAlgorithmsManager() { }
};

#endif  /* ----- #ifndef INCLUDED_PATCHING_ALGORITHMS_MANAGER_HXX_INC  ----- */

#ifndef  INCLUDED_COMMON_UPDATE_HXX_INC
#define  INCLUDED_COMMON_UPDATE_HXX_INC

#include	"gateway_export.hxx"
#include	"Version.hxx"
#include	<string>
#include	<vector>

//module_name, oldversion, newversion
struct UPDATER_COMMON_SRC_EXPORT_IMPORT Update
{
    Update () { }
    Update (std::string const &module_name,
            Version const &old_version,
            Version const &new_version)
        :
            module_name(module_name),
            old_version(old_version),
            new_version(new_version) { }

    Update (Update const &u)
        : module_name(u.module_name), old_version(u.old_version),
        new_version(u.new_version) { }

    Update &operator= (Update const &u)
    {
        if (this == &u) return *this;
        module_name = u.module_name;
        old_version = u.old_version;
        new_version = u.new_version;
        return *this;
    }

    std::string module_name;
    Version old_version, new_version;

    void serializeInto (std::ostream &out) const;
    static bool unserializeFrom (std::istream &in, Update &update);
}; 

//@TODO: boost::interprocess::vector for binary compatibility
typedef std::vector<Update> UpdatesVector;

#endif   /* ----- #ifndef INCLUDED_COMMON_UPDATE_HXX_INC  ----- */

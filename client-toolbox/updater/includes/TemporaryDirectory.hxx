#ifndef  INCLUDED_COMMON_TEMPORARYDIRECTORY_HXX_INC
#define  INCLUDED_COMMON_TEMPORARYDIRECTORY_HXX_INC

#include	<string>
#include	"gateway_export.hxx"

//! This class creates a temporary directory depending on the current platform.
//! The directory name can be retrieved with getPath(). Normally, this
//! directory is automatically deleted when the TemporaryDirectory object is
//! destroyed (RAII). If you want to keep it after the directory is destroyed,
//! you must explicitly call keepIt() on the object.
class UPDATER_COMMON_SRC_EXPORT_IMPORT TemporaryDirectory

{
    public:
        TemporaryDirectory ();
        ~TemporaryDirectory ();
        void keepIt();
        void deleteIt();
        std::string const &getPath() const { return path_; }
    private:
        //noncopyable
        TemporaryDirectory (TemporaryDirectory const &);
        TemporaryDirectory &operator= (TemporaryDirectory const &);

        void deleteDir();

        std::string path_;
        bool delete_on_destruction_;
};

#endif   /* ----- #ifndef INCLUDED_COMMON_TEMPORARYDIRECTORY_HXX_INC  ----- */

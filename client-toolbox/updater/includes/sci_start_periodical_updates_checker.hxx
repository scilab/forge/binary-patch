
#ifndef  INCLUDED_SCI_START_PERIODICAL_UPDATES_CHECKER_HXX_INC
#define  INCLUDED_SCI_START_PERIODICAL_UPDATES_CHECKER_HXX_INC

#include	"gateway_export.hxx"

extern "C" UPDATER_GATEWAY_EXPORT
int sci_start_periodical_updates_checker (char *fname);

#endif

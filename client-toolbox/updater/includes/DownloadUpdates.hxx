
#ifndef  DOWNLOAD_UPDATES_HXX_INC
#define  DOWNLOAD_UPDATES_HXX_INC

#include	<string>
#include	"Update.hxx"
#include	"gateway_export.hxx"

UPDATER_IMPL_SRC_EXPORT_IMPORT
void download_updates (UpdatesVector const &updates);

UPDATER_IMPL_SRC_EXPORT_IMPORT
void throw_if_downloaded_unapplied_updates();

#endif   /* ----- #ifndef DOWNLOAD_UPDATES_HXX_INC  ----- */

#ifndef  INCLUDED_UPDATER_UPDATERERRORCODES_HXX_INC
#define  INCLUDED_UPDATER_UPDATERERRORCODES_HXX_INC

#include	"common.hxx"
#include	"gateway_export.hxx"

namespace UpdaterErrorCodes
{
    enum
    {
        Ok = 0,

        CouldNotStartScilab,

        CouldNotGetTempPath,
        CouldNotGetAppdataDirectory,
        CouldNotLocateHomeDirectory,

        //harddisk i/o
        CouldNotOpenFile, // str param
        CouldNotCreateDirectory, // str param
        CouldNotWriteToFile, // str param
        //CouldNotGetFileExclusiveLock, // str param

        //deserialization
        VersionDeserializationFailed,
        UpdateDeserializationFailed,

        //decompressing
        ExtractingError, // str param (the error message of libarchive)
        CouldNotDecompress_CouldNotSpawnStdinPipe,
        CouldNotDecompress_CouldNotSpawnStdoutPipe,
        CouldNotDecompress_CouldNotSpawnTarProcess,
        //int param (bytes written), int param (total amount of bytes)
        CouldNotDecompress_CouldNotWriteToTarStdin,
        CouldNotDecompress_TarReturnedNonzeroExitCode,

        NoPatchingAlgorithmsAvailable,
        NoNewUpdates,
        CouldNotGetVersionOfModule, // str param (module name)
        //str param (algorithm name), str param (filename)
        CouldNotApplyUpdate,

        CouldNotParseUpdatesFile_NoUpdateId, // int param (line number)
        CouldNotParseUpdatesFile_NoFilename, // int param (line number)
        CouldNotParseUpdatesFile_NoPatchFilename, // int param (line number)

        //You have downloaded updates which were not applied. Please apply
        //downloaded updates before attempting to check for updates or download
        //updates again.
        HaveDownloadedUnappliedUpdates,
        
        // this error code is used to describe a CurlException disguised as an
        // UpdaterException. This is used by the updater thread to 
        // str param (the .what() of the CurlException object)
        ForwardCurlException
    };
}

UPDATER_IMPL_SRC_EXPORT_IMPORT
std::string message_from_exception (UpdaterException const &ec);
#endif   /* ----- #ifndef INCLUDED_UPDATER_UPDATERERRORCODES_HXX_INC  ----- */

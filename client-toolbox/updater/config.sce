TOOLBOX_NAME = "updater";
TOOLBOX_TITLE = "Scilab automatic updates";
PATCHING_ALGORITHMS = [
            'SendWholeFile' ..
            'Text' ..
            'Bsdiff' ..
            'DeleteFile' ..
            //'xdelta' ..
            //'windows2k_patchapi' ..
            //'courgette' ..
            ];

if getos() == 'Windows' then
    PATCHING_ALGORITHMS = [ PATCHING_ALGORITHMS ..
        'Courgette' ..
    ];
end

//BOOST_INC_DIR = 'D:/boost';
//BOOST_LIB_DIR = 'D:/boost/stage/lib';
BOOST_INC_DIR = '/home/stefan/boost_1_47_0/';
BOOST_LIB_DIR = '/home/stefan/boost_1_47_0/stage/lib/';
//it would be nice if we could deduce these automatically
COMPILER = 'gcc'; // allowed values are msvc and gcc
UPDATER_PLATFORM_STRING = 'l64'; // allowed values are: w86,w64,l86,l64,m
//(lowercase first letter)

if COMPILER == 'msvc' then
    UPDATER_BOOST_LDFLAGS = ' /LIBPATH:' + BOOST_LIB_DIR;
else
    UPDATER_BOOST_LDFLAGS = ' -L' + BOOST_LIB_DIR;
end

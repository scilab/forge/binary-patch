import os.path
import re
import argparse
import sys
from dependency_tree_base import *

args = argparse.ArgumentParser(description='Dependency tree generator')
args.add_argument('-f','--from', nargs=1, required=False, action = 'append',
		dest='from_', metavar = 'project', help='Show only the direct
		dependencies of `project\'')
args.add_argument('-t','--to', nargs=1, required=False, action='append',
		dest='to', metavar = 'project', help='Show only the projects that
		directly depend on `project\'')
args.add_argument('-l','--list', action='store_true', dest = 'list_',
		help='List the project names and exit')
args.add_argument('--label', action='store_true', help='Labels the arcs',
		default=False)
args.add_argument('-m', '--match', action='append', required=False, nargs=1,
		dest='match', metavar='project', help='Equivalent to -f project -t
		project')
args.add_argument('-r', '--recurse', action='store_true', default=False,
		dest='recurse', help='By default, when filtering options are used, this
		script only' 'displays direct dependencies. This overrides the
		behavior, making it' 'display indirect dependencies too')
args = args.parse_args()

#TODO: implement --recurse
if args.recurse:
	print 'Error: --recurse is not implemented.'
    sys.exit(-1)

if args.label:
	REFERENCE_FLAGS += ',label="references"'
	LINK_FLAGS += ',label="links to"'


root = None
from_ = []
to = []
print_all = True
#recurse = args.recurse
if args.from_ or args.to or args.match:
	if args.from_:
		from_ = [i[0].lower() for i in args.from_]
		root = args.from_[0]
	if args.to:
		to = [i[0].lower() for i in args.to]
		if not root:
			root = args.to[0]
	if args.match:
		match = [i[0].lower() for i in args.match]
		from_ += match
		to += match
	print_all = False
	

patt = solution_getprojects_pattern
patt2 = re.compile( """\s*<ProjectReference Include="([^"]+)">""")
patt3 = re.compile(
		"""\s*<AdditionalDependencies>([^>]+)</AdditionalDependencies>""")

f=open(os.path.join(SOLUTION_DIR, SOLUTION_FILE), 'r')

def projfilter(src, dest):
	return print_all or (from_ and src.lower() in from_) \
			or (to and dest.lower() in to)

# projfile => projname
projects = {}
# projname => projfile
projects_inv = {}

for line in f.readlines():
	match = patt.match(line)
	if match == None:
		continue
	fn = to_filename(match.group(2))
	projects[fn] = match.group(1)
	projects_inv[match.group(1)] = fn
f.close()


if args.list_:
	for projfile in projects.iterkeys():
		print projfile
	sys.exit(0)

print 'digraph "Project dependencies" {'
if root != None:
	#TODO: case
	#print 'root='+root
	#print '"%s" [%s]' % (root, ROOT_FLAGS)
	pass

print 'node [%s];' % NODE_FLAGS

for projname in projects.itervalues():
	if projname.lower() in from_ or projname.lower() in to:
		print '"%s" [%s];' % (projname, ROOT_FLAGS)

for projfile, projname in projects.iteritems():
	projfile = to_filename(projfile)
	f = open(projfile, 'r')
	lines = f.readlines()
	for line in lines:
		match = patt2.match(line)
		if match == None:
			continue
		referredproj = to_filename(match.group(1),os.path.split(projfile)[0])
		try:
			dep = projects[referredproj]
			if projfilter(projname,dep):
				print '"%s" -> "%s" [%s];' % (projname, dep, REFERENCE_FLAGS)
		except KeyError:
			print 'Warning: Couldn\'t find project in solution:',referredproj
	deps = []
	for line in lines:
		match = patt3.match(line)
		if match == None:
			continue
		else:
			deps = match.group(1).split(';')
			break
	for dep in deps:
		dep = to_filename(dep, os.path.split(projfile)[0])
		dep = os.path.splitext(os.path.basename(dep))[0]
		if projects_inv.has_key(dep) and projfilter(projname,dep):
			print '"%s" -> "%s" [%s];' % (projname,dep,LINK_FLAGS)

print '}'

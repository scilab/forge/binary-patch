###########Configuration starts here########################
############################################################

SOLUTION_DIR = 'E:\\tmp\\scilab-git\\scilab\\' #trailing backslash
SOLUTION_FILE = 'scilab_f2c.sln'
REFERENCE_FLAGS = 'color=blue'
LINK_FLAGS = 'color=green'
NODE_FLAGS = 'shape=box,color=red'
ROOT_FLAGS = 'shape=box,bgcolor=red,fontcolor=white,style=filled'

## Specific to generate_dll_dependency_tree.dll
DUMPBIN_FILE = 'C:\\Program Files (x86)\\Microsoft Visual Studio 10.0\\' \
        'VC\\bin\\dumpbin.exe'
BINARY_OUTPUT_DIR = 'E:\\tmp\\scilab-git\\scilab\\bin\\wtf\\' # trailing backslash
ADDITIONAL_PATH_DIRS = [
    'C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE']
############################################################
#############Configuration ends here########################

import re, os

solution_getprojects_pattern  = re.compile(
        """Project\("[^"]+"\) = "([^"]+)",\s+"([^"]+)".+""")

def to_filename(fn, relative_to=SOLUTION_DIR):
	return os.path.normcase(os.path.normpath(os.path.join(relative_to,
		fn.replace('$(SolutionDir)',SOLUTION_DIR))))


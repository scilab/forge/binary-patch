import os.path
from os import name, environ
from sys import exit, stderr
from dependency_tree_base import *
import argparse
import subprocess
import re

def find_output_files():
    projects=[]
    outfiles=[]
    f=open(os.path.join(SOLUTION_DIR, SOLUTION_FILE), 'r')
    patt = solution_getprojects_pattern
    for line in f.readlines():
        match = patt.match(line)
        if match == None:
            continue
        projects.append ( (match.group(1),to_filename(match.group(2))) )
    f.close()
    patt2 = re.compile(
            """
            <ItemDefinitionGroup\s
            Condition="'\$\(Configuration\)\|\$\(Platform\)'
            =='Debug\|Win32'">
            (.+?)
            </ItemDefinitionGroup>""",
            re.DOTALL|re.VERBOSE)
    patt3 = re.compile("""<OutputFile>(.+?)</OutputFile>""", re.DOTALL)
    patt4 = re.compile("""<ConfigurationType>(.+?)</ConfigurationType""",
            re.DOTALL)
    for project_name, project_file in projects:
        f = open(project_file, 'r')
        data=f.read()
        match=patt2.search(data)
        match2=None
        if match:
            match2 = patt3.search(match.group(1))
        if not match or not match2:
            #print '*** Couldn\'t find output file for project %s (%s)!' \
            #        % (project_name, project_file)
            continue
        match3=patt4.search(data)
        TargetExt = '.dll'
        if not match3:
            stderr.write( '*** Couldn\'t find project type for project %s (%s)!\n'
                    % (project_name, project_file) )
        elif match3.group(1) == 'Application':
            TargetExt = '.exe'
        elif match3.group(1) == 'DynamicLibrary':
            TargetExt = '.dll'
        elif match3.group(1) == 'StaticLibrary':
            TargetExt = '.lib'
        else:
            stderr.write( '*** Unknown project type for project %s (%s): %s\n' \
                    % (project_name, project_file, match3.group(1)) )

        #@TODO: OutDir, IntDir et al?
        binary = match2.group(1).replace('$(ProjectName)', project_name) \
            .replace('$(TargetName)', project_name) \
            .replace('$(TargetExt)', TargetExt)
        binary = to_filename(binary, BINARY_OUTPUT_DIR)
        #print 'Found output file for project %s (%s): %s' % (project_name,
        #        project_file, binary)
        outfiles.append(binary)
    return outfiles


if __name__ == '__main__':
    if os.name != 'nt':
        stderr.write( 'This script only works on windows' )
        sys.exit(-1)

    args = argparse.ArgumentParser(description='DLL Dependency tree generator')

    ##@TODO: IMPL FROM AND TO!!!
    args.add_argument('-i', '--ignore', nargs=1, required=False,
        action='append', dest='ignore', metavar='DLL file', help='Ignore the '
        'file named DLL file')
    args.add_argument('-l', '--list', action='store_true', dest='list_',
            help='List the DLL/EXE file names and exit')

    args = args.parse_args()

    binaries = find_output_files()

    if args.list_:
        print '\n'.join(map(os.path.basename,binaries))
        exit(0)

    ignore = []
    if args.ignore:
        ignore = [i[0].lower() for i in args.ignore]

    environ['PATH'] += ';' + ';'.join(ADDITIONAL_PATH_DIRS)

    dllpatt = re.compile("""^\s+([^ ]+\.dll)""", re.IGNORECASE)

    print 'digraph "DLL dependencies" {'
    print 'node [%s];' % NODE_FLAGS

    for binary in binaries:
        if binary.lower() in ignore:
             continue

        if not os.path.isfile(binary):
            stderr.write('*** Inexistent file: %s\n' % binary)
            exit(-1)
         
         # get exports and imports

        """
         print 'Exports on',binary
         
         fh = subprocess.Popen([DUMPBIN_FILE, '/EXPORTS', binary], bufsize=1,
                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
         exports = []
         for line in fh.stdout:
             print '::',line
        """

        fh = subprocess.Popen([DUMPBIN_FILE, '/IMPORTS', binary], bufsize=1,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        for line in fh.stdout:
            match = dllpatt.match(line)
            if match and match.group(1).lower() not in ignore:
                print '"%s" -> "%s" [%s];' % (os.path.basename(binary).lower(),
                        match.group(1).lower(), LINK_FLAGS)
    print '}'



statistical_analysis.py works by generating an .ods (opendocument spreadsheet)
which contains a table structured as follows: each row in the table represents
a binary file that has been patched each column in the table represents a
binary patching algorithm a table cell will contain the size of the patch for
the binary file indicated by the row and the algorithm indicated by the column.

After this table is generated, statistical information can be computed, such as
how well has each algorithm performed overall, on average, for which percentage
of the file has it performed best/worst, same stats applied for various
extensions (e.g. average for .dll) etc.

E.g.:

# vim:ts=4:sts=4:sw=4

		left_size right_size	xdelta		bsdiff
file1	 1.3MB		3.6MB		 3.2KB       3.8KB
file2	 2.5MB		3.2MB		 2.4KB		 1.5KB

xdelta avg: 2.8KB
bsdiff avg: 2.65KB
xdelta max: 3.2KB
bsdiff max: 3.8KB
xdelta performed best: 50% of the time
bsdiff performed best: 50% of the time
etc.

